var sendSMS = function(elem){
	var data = {
		mobile:elem.parentElement.children[1].value,
		id:elem.parentElement.children[2].value,
		message:elem.parentElement.children[3].children[0].value,
		_token: $('meta[name="csrf-token"]').attr('content')
	}

	$.ajax({
	  type: "POST",
	  url: '/sms/'+data.id,
	  data: data,
	  success: function(output){
	  	$("#error").hide();
	  	$('#success').html('Message sent successfully!');
	  	$('#success').css('display','block');
	  },
	  error:function(errors){
	  	$("#success").hide();
	  	var list="";
		$.each(errors.responseJSON.errors, function(key, value) {

		list += "<ul>";
		list += '<li>' + value[0] + '</li>'; //showing only the first error.
		});

	  	$('#error').html(list);
	  	$('#error').show();
	  }
	});

}


function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
