<?php

namespace Tests\Feature;

use Twilio;
use \Mockery;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SMSTest extends TestCase
{
    /**
    * Setup the test environment.
    *
    * @return void
    */
    protected function setUp(){
        parent::setUp();
        Twilio::shouldReceive('message')
        ->with('132132132','test_message');
    }

    /**
     * @test
     * @return void
     */
    public function it_can_send_an_sms_message()
    {

        $user = factory(User::class)->create([
            'mobile' => '132132132',
        ]);


        $data = [
            'id' => $user->id,
            'mobile' => '132132132',
            'message' => 'test_message'
        ];

        $this->post("/sms/{$user->id}",$data)->assertStatus(200);
    }
    
    /**
     * @test
     * @return void
     */
    public function paid_users_only_will_receive_sms()
    {
        // 2 users paid and not

        $notPaidUser = factory(User::class)->create([
        'mobile' => '132132123123132',
        'paid'=>false
        ]);

        $paidUser = factory(User::class)->create([
        'mobile' => '132132132',
        'paid'=>true
        ]);


        $admin = factory(User::class)->create(['is_admin'=>true]);
        $this->be($admin);

        $data = [
            'id' => $paidUser->id,
            'mobile' => '132132132',
            'message' => 'test_message'
        ];

        $this->post("/sms/{$paidUser->id}",$data)->assertStatus(200);

        $this->expectException(ValidationException::class);
        Twilio::shouldReceive('message')
        ->with('132132123123132','test_message');

        $data = [
            'id' => $notPaidUser->id,
            'mobile' => '132132123123132',
            'message' => 'test_message'
        ];
        
        $this->post("/sms/{$notPaidUser->id}",$data);   
    }

    /**
     * @test
     * @return void
     */
    public function only_admins_can_send_sms()
    {
        // 3 users guest , user, admin
        $userWillReceiveAMessage = factory(User::class)->create(['mobile' =>'132132132']);

         $data = [
            'id' => $userWillReceiveAMessage->id,
            'mobile' => '132132132',
            'message' => 'test_message'
        ];
        
        $this->expectException('Illuminate\Auth\Access\AuthorizationException');

        $user = factory(User::class)->create(['is_admin'=>false]);
        $this->be($user);
        
        $this->post("/sms/{$userWillReceiveAMessage->id}",$data);

        \Auth::logOut();

        $admin = factory(User::class)->create(['is_admin'=>true]);
        $this->be($admin);
        $this->post("/sms/{$userWillReceiveAMessage->id}",$data)->assertStatus(200);
    }

}
