<?php

namespace Tests\Feature;
use DB;
use Cookie;
use \Mockery;
use App\User;
use Carbon\Carbon;
use Tests\TestCase;
use App\Payment\PayMobWrapper;
use BaklySystems\PayMob\Facades\PayMob;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PaymentTest extends TestCase
{
	use RefreshDatabase;

	public $user;
	public $auth;

	/**
	* Setup the test environment.
	*
	* @return void
	*/
    protected function setUp(){
		parent::setUp();
    	$this->user = factory(User::class)->raw([
    		'name' =>'test name',
    		'password_confirmation' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm',
    	]);
    }

    /**
     * @test
     * @return void
     */
    public function it_can_register_a_user_after_the_payment()
    {
		$knownDate = Carbon::create(2020, 5, 21, 12); // create testing date
		Carbon::setTestNow($knownDate);

    	$this->paymentAuth(false);

		$service = Mockery::mock(PayMobWrapper::class);

		$this->app->instance(PayMobWrapper::class, $service);

		$service->shouldReceive('setPayLoad')
		->matchArgs($this->user)
		->passthru();

		$service->shouldReceive('getPaymentUrl')
		->with($this->auth)
		->andReturn('url')
		->passthru();

		$service->shouldReceive('registerUser')
		->matchArgs($this->user)
		->passthru();

		$service->shouldReceive('generateUserLink')
		->andReturn('AFFILIATEID')
		->once();

		$service->shouldReceive('getCost')
		->andReturn('100')
		->once();

		$service->shouldReceive('makeOrder')
		->with($this->auth,'100')
		->andReturn((object)['id' =>7777])
		->once();

		$service->shouldReceive('getPaymentKey')
		->matchArgs([$this->auth,'100',(object)['id' =>7777],$this->user])
		->andReturn((object)['token' =>'payment_key'])
		->once();
		try{
			
		$this->post('/payment',$this->user+['token' =>'aksjdoauioxzcn']);
		}catch(ValidationException $e ){
		    dd($e->errors());
		}


    	$expirationDay = Carbon::now();
		$expirationDay = $expirationDay->addDays(30);

    	
    	$this->assertDatabaseHas('users',[
  			'name' => $this->user['name'],
			'country' => $this->user['country'],
			'state' => $this->user['state'],
			'city' => $this->user['city'],
			'mobile' => $this->user['mobile'],
			'email' => $this->user['email'],
			'zipcode' => $this->user['zipcode'],
			'plan' => $this->user['plan'],
			'expiration' => $expirationDay->toDateTimeString()
    	]);
    }


     /**
     * @test
     * @return void
     */
    public function user_cannot_use_trial_version_more_than_once()
    {
    	$user = $this->paymentAuth();

    	$firstPayment = Mockery::mock(PayMobWrapper::class);

		$this->app->instance(PayMobWrapper::class, $firstPayment);
    	
		$firstPayment->shouldReceive('setPayLoad')
		->matchArgs($user)
		->passthru();

		$firstPayment->shouldReceive('getPaymentUrl')
		->with($this->auth)
		->andReturn('url')
		->once();

		$secondPayment = Mockery::mock('PayMobWrapper');

		$secondPayment->shouldNotReceive('getPaymentUrl');


		$response1 = $this->post('/payment',$user)
  		->assertStatus(302)
  		->assertRedirect('/url')
  		->assertPlainCookie('usedTrialPlan');


		$response2 = $this->call('POST','/payment',$user,['usedTrialPlan' =>'true'])
  		->assertStatus(302)
  		->assertRedirect('/payment');
  	
    }
	public function paymentAuth($makeUser=true){
	
		$this->auth =(object)[
			'token'=>'ASaxxe234ASKLJ',
			'profile'=>(object)['id'=>1]
		];

		PayMob::shouldReceive('authPaymob')->andReturn($this->auth);
		
		if(!$makeUser){return;}

		return factory(User::class)->raw([
			'plan' => 'trial',
			'name' => 'kamel',
			'password' =>'123456',
			'password_confirmation' =>'123456',

		]);
	}

}
