<?php

namespace Tests\Feature;
use App\User;
use Tests\TestCase;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Testing\RefreshDatabase;
class UserRegisterTest extends TestCase
{
	use RefreshDatabase;

    /**
     * @test
     * @return void
     */
    public function it_can_register()
    {
        $this->withoutExceptionHandling();
        $data = [
        	'name'=>'mohamed',
            'password'=>'123456',
        	'password_confirmation'=>'123456',
        	'email'=>'m1@m.com',
            'mobile'=>'12345678900'
        ];

        $this->post('/register',$data);   
      	               
        $this->assertDatabaseHas('users',[
        	'name' => 'mohamed',
        	'email' => 'm1@m.com',
            'mobile'=>'12345678900'	
        ]);
    }

    /**
     * @test
     * @return void
     */
    public function user_with_expired_date_cannot_access()
    {
        $knownDate = Carbon::create(2020, 5, 21, 0); // create testing date
        Carbon::setTestNow($knownDate);
       
        $userCanAccess = factory(User::class)->create([
            'expiration' => '2021-05-21 00:00:00',
            'password' => Hash::make('123465'),
            'paid' =>1
        ]);

        $data=[
            'email' => $userCanAccess->email,
            'password' => '123465'
        ];
        
        // $this->post('/login',$data)->assertRedirect('/home');

        $userCannotAccess = factory(User::class)->create([
            'expiration' => '2020-05-20 23:59:59',
            'password' => Hash::make('123465'),
            'paid' =>0
        ]);

        $data=[
            'email' => $userCannotAccess->email,
            'password' => '123465'
        ];
        
        \Auth::logout();
     
        $this->post('/login',$data)->assertRedirect('/login');

    }

}
