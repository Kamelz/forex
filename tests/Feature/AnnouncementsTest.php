<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AnnouncementsTest extends TestCase
{

    use RefreshDatabase;
    
    /** @test */
    public function only_admin_can_view_announcements(){

        $user = factory(User::class)->create(['is_admin'=>0,'paid'=>1,'verified' =>1]);
        $this->be($user);
        // if the current user is not an admin then redirect
        $this->get('/announcements')
        ->assertStatus(302)
        ->assertRedirect('/home');
    }



    /** @test */
    public function only_admin_can_update_announcements()
    {

        $user = factory(User::class)->create(['is_admin'=>0,'paid'=>1,'verified' =>1]);
        $this->be($user);

        $data = [
            'title'=>'test_title',
            'body'=>'test_body',
            'link'=>'test_link',
            'note'=>'test_note',
        ];
        $this->post('/announcements',$data)
        ->assertStatus(302)
        ->assertRedirect('/home');
        
        $admin = factory(User::class)->create(['is_admin'=>1,'paid'=>1,'verified' =>1]);
        $this->be($admin);
        
        
        $this->post('/announcements',$data)
        ->assertStatus(302)
        ->assertRedirect('/announcements');


        $this->assertDatabaseHas('announcements',[
            'title'=>'test_title',
            'body'=>'test_body',
            'link'=>'test_link',
            'note'=>'test_note',
        ]);
    }
}
