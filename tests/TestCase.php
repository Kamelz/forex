<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
	* Setup the test environment.
	*
	* @return void
	*/
    protected function setUp(){
		parent::setUp();
		$this->withoutExceptionHandling();	

    }
}
