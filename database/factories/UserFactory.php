<?php

use Faker\Generator as Faker;
use App\User;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'address' => $faker->word,
        'country' => $faker->word,
        'city' => $faker->word,
        'state' => $faker->word,
        'zipcode' => $faker->randomNumber(),
        'plan' => 'pro',
        'mobile' => '01002260354',
        'expiration' => null,
        'affiliate_id' => $faker->word,
        'referred_by' => null,
        'verified' => 0,
        'paid' => 0,
        'remember_token' => str_random(10),
    ];
});
