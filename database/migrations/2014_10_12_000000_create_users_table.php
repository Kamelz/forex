<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('affiliate_id')->unique();  
            $table->integer('zipcode')->nullable();
            $table->string('password');      
            $table->string('email')->unique();
            $table->string('address')->nullable();
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('paypal_payment_id')->nullable();
            $table->string('mobile');
            $table->string('plan')->nullable();
            $table->dateTime('expiration')->nullable();
            $table->boolean('verified')->default(false);
            $table->boolean('paid')->default(false);
            $table->boolean('is_admin')->default(false);
            $table->rememberToken();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
