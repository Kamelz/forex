<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     
     * @return void
     */
    public function run()
    {
        User::create([
            'name'         => 'admin',
            'password' => Hash::make('+O=^F1YJe;kT'),
            'country'      => '',
            'state'        => '',
            'city'         => '',
            'mobile'       => 0,
            'email'        => 'admin@admin.com',
            'address'      => '',
            'zipcode'      => 0,
            'plan'         => '',
            'expiration'   => null,
            'affiliate_id' => '1111111',
            'is_admin'     => 1,
            'verified'     => 1,
            'paid'         => 1,
        ]);
    }
}
