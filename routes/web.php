<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/switch/{lang}', 'LanguageController@switch')->name('switch');

Route::get('/users/paid', 'ApiController@getPaidUsers');

Route::group(['middleware' => ['auth']], function () {
	Route::group(['middleware' => ['admin']], function () {
		Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
	});
});

Route::get('/test',function(){

//Log::emergency('The system is down!');
});

Auth::routes();
Route::get('/logout', 'Auth\AuthController@logout');

Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');

Route::get('/payment', 'PaymentController@index');

Route::get('/response', 'PayMobController@invoice');

Route::post('/processed-callback', 'PayMobController@processedCallback')->name('processedCallback');

Route::post('/payment', 'PayPalController@checkingOut')->name('payment');
Route::get('status', 'PayPalController@getPaymentStatus');

<<<<<<< HEAD
Route::group(['middleware' => ['auth']], function () {
	Route::group(['middleware' => ['verified']], function () {


		Route::get('/affiliate', 'HomeController@showAffiliate');

		Route::group(['middleware' => ['paid']], function () {	

			Route::get('/', 'HomeController@index')->name('home');
			Route::get('/home', 'HomeController@index')->name('home');
			Route::get('/calender', 'HomeController@showCalender');
			Route::get('/trend-meters', 'HomeController@showTrendMeters');
			Route::get('/forex-news', 'HomeController@showNews');
			Route::get('/settings', 'HomeController@showSettings');
			Route::group(['middleware' => ['admin']], function () {	
				Route::get('/announcements', 'AnnouncementController@index');
				Route::post('/announcements', 'AnnouncementController@store');
			});
=======


Route::group(['middleware' => ['auth']], function () {
	Route::group(['middleware' => ['verified']], function () {


		Route::get('/affiliate', 'HomeController@showAffiliate');

		Route::group(['middleware' => ['paid']], function () {	
>>>>>>> bbf781e14753a6eaed61d8726a484ea66e8edee6

			Route::get('/', 'HomeController@index')->name('home');
			Route::get('/home', 'HomeController@index')->name('home');
			Route::get('/calender', 'HomeController@showCalender');
			Route::get('/trend-meters', 'HomeController@showTrendMeters');
			Route::get('/forex-news', 'HomeController@showNews');
			Route::get('/settings', 'HomeController@showSettings');
			Route::group(['middleware' => ['admin']], function () {	
				Route::get('/announcements', 'AnnouncementController@index');
				Route::post('/announcements', 'AnnouncementController@store');
			});

<<<<<<< HEAD
			Route::get('/affiliate/payback', 'PaymentController@payAffiliateByEmail');

			Route::get('/sms', 'SMSController@index')->name('sms')->name('sms.send')
			->middleware('can:send,App\User');

			Route::post('/sms/{id}', 'SMSController@send')
			->name('sms.send')
			->middleware('can:send,App\User');

=======

			Route::get('/affiliate/payback', 'PaymentController@payAffiliateByEmail');

			Route::get('/sms', 'SMSController@index')->name('sms')->name('sms.send')
			->middleware('can:send,App\User');

			Route::post('/sms/{id}', 'SMSController@send')
			->name('sms.send')
			->middleware('can:send,App\User');

>>>>>>> bbf781e14753a6eaed61d8726a484ea66e8edee6
			Route::post('/sms', 'SMSController@sendToAll')
			->name('sms.send.all')
			->middleware('can:send,App\User');
		});
	});
});

