<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AffiliatePayment extends Model
{
    protected $fillable = [
		'affiliate_id',
		'user_id',
		'paid'
    ];

       public function affiliatePayment(){

        return $this->hasMany('App\User','id','user_id');
    }

}
