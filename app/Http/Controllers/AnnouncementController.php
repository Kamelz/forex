<?php

namespace App\Http\Controllers;

use App\Announcement;
use Illuminate\Http\Request;

class AnnouncementController extends Controller
{

    public function index()
    {
        return view('announcement');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'nullable',
            'body'  => 'nullable',
            'link'  => 'nullable',
            'note'  => 'nullable',
        ]);

       $announcements = Announcement::first();

        if(!$announcements){

            Announcement::create($validatedData);
        
        }else{

            $announcements->update($validatedData);
        }
        return redirect('/announcements')->with('status', 'Announcements updated successfully!!');
    }
}
