<?php

namespace App\Http\Controllers;

use Twilio;
use App\User;
use App\Http\Requests\SendSMS;
use Illuminate\Http\Request;

class SMSController extends Controller
{
    public function index(){

    	$users = User::where('paid',1)->where('is_admin',0)->get();
    	return view('sms')->with(['users'=>$users]);
    }

    public function send(User $user,SendSMS $request){

    	Twilio::message($request->mobile, $request->message);

    	return response('Sent', 200);
    }

    public function sendToAll(Request $request){

    	$validatedData = $request->validate([
        'message' => 'required',
	    ]);

    	$users = User::where('paid',1)->where('is_admin',0)->get();
    	foreach ($users as $user) {
    		Twilio::message($user->mobile, $request->message);	
    	}
    	return redirect()->back()->with('status', 'Messages sent successfully!!');
    }
}
