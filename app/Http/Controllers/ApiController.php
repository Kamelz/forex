<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function getPaidUsers(){
       return User::where('paid',1)->where('verified',1)->where('is_admin',0)->get()->pluck('email');
    }
}
