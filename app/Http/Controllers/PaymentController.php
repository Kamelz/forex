<?php
namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use App\Traits\UserTrait;
use Illuminate\Http\Request;
use App\Payment\PaymentGateway;
use Illuminate\Support\Facades\Hash;
use PayPal;
class PaymentController extends Controller
{
	use UserTrait;

	public function index(){

		return view('payment');
	}
	
	public function pay(Request $request){

		$validatedData = $request->validate([
			'plan' => 'required',
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'mobile' => 'required|numeric|digits:11',
            'password' => 'required|string|min:6|confirmed',
			'address' => 'required',
			'counry' => 'required',
			'state' => 'required',
			'city' => 'required',
			'zipCode' => 'required',
		]);


		$payment = new PaymentGateway();
	
		$charge = $payment->charge($request);

		$user = $this->registerUser($request);

		$this->payAffiliateBack($payment,$user);
		
		return view('thanks')->with(['charge'=>$charge]);

	}
	public function registerUser($request){

		$expirationDay = Carbon::now();
		if($request['plan'] === 'trial'){
            $expirationDay = $expirationDay->addDays(7);
        }
        else{
            $expirationDay = $expirationDay->addDays(30);
        }

		$affiliateId = $this->generateUserLink();
		return User::create([
			'name' => $request['name'],
			'password' => Hash::make($request['password']),
			'country' => $request['country'],
			'state' => $request['state'],
			'city' => $request['city'],
			'mobile' => $request['mobile'],
			'email' => $request['email'],
			'address' => $request['address'],
			'zipcode' => $request['zipcode'],
			'plan' => $request['plan'],
			'expiration' => $expirationDay,
			'affiliate_id' => $affiliateId
		]);
	}

	public function payAffiliateBack($payment, User $user){
	
		$ref = request()->cookie('ref');
	
		if(!$ref){return;}
		if(!User::where('affiliate_id',$ref)->count()){return;}

		$refId = User::where('affiliate_id',$ref)->firstOrFail()->id;
		$user->referred_by = $refId;
		$user->save();
		$payment->payAffiliate();
	}


	public function payAffiliateByEmail(User $user){

			$provider =	PayPal::setProvider('adaptive_payments');
        
			$data = [
			    'receivers'  => [
			        [
			            'email' => 'johndoe@example.com',
			            'amount' => 10,
			            'primary' => false,
			        ]
			    ],
			    'payer' => 'EACHRECEIVER', // (Optional) Describes who pays PayPal fees. Allowed values are: 'SENDER', 'PRIMARYRECEIVER', 'EACHRECEIVER' (Default), 'SECONDARYONLY'
			    'return_url' => url('payment/success'), 
			    'cancel_url' => url('payment/cancel'),
			];

			return $provider->createPayRequest($data);

	}


}