<?php

namespace App\Http\Controllers;

use App\Announcement;
use App\User;
use Illuminate\Http\Request;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $announcements= Announcement::first();
        return view('home')->with(['announcements' => $announcements]);
    }


    public function showCalender()
    {
        return view('calender');
    }


    public function showTrendMeters()
    {
        return view('meter');
    }


    public function showNews()
    {
        return view('news');
    }

    public function showSettings()
    {
        return view('settings');
    }

    public function showAffiliate()
    {
        return view('affiliate');
    }

}
