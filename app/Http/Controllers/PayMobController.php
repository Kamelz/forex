<?php

namespace App\Http\Controllers;
use DB;
use Mail;
use PayPal;
use Cookie;
use App\User;
use Carbon\Carbon;
use ErrorException;
use App\VerifyUser;
use App\Mail\VerifyMail;
use App\AffiliatePayment;
use App\Traits\UserTrait;
use Illuminate\Http\Request;
use App\Payment\PayMobWrapper;
use BaklySystems\PayMob\Facades\PayMob;
class PayMobController extends Controller
{
    use UserTrait;
    /**
     * Display checkout page.
     *
     * @return Response
     */
    public function checkingOut(Request $request,PayMobWrapper $payMobWrapper)
    {
        $validatedData = $request->validate([
            'plan' => 'required',
            'name' => 'required|string|max:255|regex:/^[a-zA-Z ]+$/',
            'email' => 'required|string|email|max:255|unique:users',
            'mobile' => 'required|numeric|digits:11',
            'password' => 'required|string|min:6|confirmed',
            'address' => 'required|string|max:255|regex:/^[a-zA-Z ]+$/',
            'country' => 'required|string|max:255|regex:/^[a-zA-Z ]+$/',
            'state' => 'required|string|max:255|regex:/^[a-zA-Z ]+$/',
            'city' => 'required|string|max:255|regex:/^[a-zA-Z ]+$/',
            'zipcode' => 'required|integer|min:0',
        ]);

        $payMobWrapper->setPayLoad($validatedData);

        $userUsedFreePlanBefore = Cookie::get('usedTrialPlan');

        if(!is_null($userUsedFreePlanBefore) && $validatedData['plan']=='trial'){

            return redirect('/payment')->withErrors(['plan'=>'Sorry you cannot use this plan more the once.']);  
          }
          
        $cookie='';

        if(is_null($userUsedFreePlanBefore) && $validatedData['plan']=='trial'){    
           $cookie = cookie('usedTrialPlan', 'true', 43800);//one month
        }

        $auth = PayMob::authPaymob(); 
 
      // login PayMob servers
        if(!isset($auth)){
          return view('payment')->with([
                        'title' => 'Authentication failed',
                        'message' => 'Error while authenticating.'
                    ]);
        }
        if (property_exists($auth, 'detail')) { // login to PayMob attempt failed.
            return view('payment')->with([
                'title' => 'Authentication failed',
                'message' => 'Error while authenticating.'
            ]);
        } 

       $url = $payMobWrapper->getPaymentUrl($auth);
      
       if(!$cookie){
        return redirect($url);
      }
        return redirect($url)->withCookie($cookie);
    }

    /**
     * Processed callback from PayMob servers.
     * Save the route for this method in PayMob dashboard >> processed callback route.
     *
     * @param  \Illumiante\Http\Request  $request
     * @return  Response
     */
    public function processedCallback(Request $request)
    { 
        $orderId = $request['obj']['order']['id'];
        $order   = config('paymob.order.model', 'App\Order')::wherePaymobOrderId($orderId)->first();

        // Statuses.
        $isSuccess  = $request['obj']['success'];
        $isVoided   = $request['obj']['is_voided'];
        $isRefunded = $request['obj']['is_refunded'];

        if ($isSuccess && !$isVoided && !$isRefunded) { // transcation succeeded.
            $this->succeeded($order);
        } elseif ($isSuccess && $isVoided) { // transaction voided.
            $this->voided($order);
        } elseif ($isSuccess && $isRefunded) { // transaction refunded.
            $this->refunded($order);
        } elseif (!$isSuccess) { // transaction failed.
            $this->failed($order);
        }

        return response()->json(['success' => true], 200);
    }

    /**
     * Display invoice page (PayMob response callback).
     * Save the route for this method to PayMob dashboard >> response callback route.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Response
     */
    public function invoice(Request $request)
    {       
        $auth = PayMob::authPaymob();

          try {
            $transaction =  PayMob::getTransaction(
                $auth->token, 
                $request->id // PayMob transaction id.
            );
        } catch (Exception $e) {
            return view('error',[
                'title'=>'Transaction Declined.',
                'message'=>'Sorry something went wrong with the transaction.'
            ]);
            }
            $this->makeUserPaid($transaction);
            $this->payAffiliateBack($transaction);
       return view('transaction',compact('transaction'));
    }


    public function makeUserPaid($transaction){
 
        $user = User::where('email',$transaction->order->shipping_data->email)->first();

        $checkUserExists = $user->orders()->where('paymob_order_id',$transaction->order->id)->exists();
        
        if($checkUserExists){
            $user->paid = 1;
            $user->save();
        }
    }

      public function payAffiliateBack($transaction){
  

      $user = User::where('email',$transaction->order->shipping_data->email)->first();

      $affiliate = User::whereId($user->referred_by)->first();

      if(!$affiliate){return;}

      if($user->affiliate->affiliate_id === $affiliate->id && $user->affiliate->paid === 1)
      {
        return;
      }
      
      $this->payAffiliateByEmail($affiliate,$user);
      return;
    }


  public function payAffiliateByEmail(User $affiliate,User $user){

    $provider = PayPal::setProvider('adaptive_payments');

    $data = [
      'receivers'  => [
        [
        'email' => $affiliate->email,
        'amount' => 1,
        'primary' => false,
        ]
      ],
      'payer' => 'EACHRECEIVER', // (Optional) Describes who pays PayPal fees. Allowed values are: 'SENDER', 'PRIMARYRECEIVER', 'EACHRECEIVER' (Default), 'SECONDARYONLY'
      'return_url' => url('home'),//success  
      'cancel_url' => url('home'),//cancel 
    ];

    $response = $provider->createPayRequest($data);
    $redirect_url = $provider->getRedirectUrl('approved', $response['payKey']);

    Mail::to($affiliate->email)->send(new AffiliateMail($redirect_url));

    if(!count(Mail::failures())){

      $payment = AffiliatePayment::where('affiliate_id',$affiliate->id)
      ->where('user_id',$user->id)
      ->first();
      $payment->paid =true;
      $payment->save();
    
    }
    return $response;
  }

}
