<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;

class LanguageController extends Controller
{

    public function switch($lang){
    	
		App::setLocale($lang);

    	return redirect()->back();
    }
}
