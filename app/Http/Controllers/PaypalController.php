<?php

namespace App\Http\Controllers;

use DB;
use URL;
use Mail;
use PayPal;
use Cookie;
use Session;
use Redirect;
use App\User;
use Carbon\Carbon;
use ErrorException;
use App\VerifyUser;
use PayPal\Api\Item;
use PayPal\Api\Payer;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Payment;
use App\Mail\VerifyMail;
use PayPal\Api\ItemList;
use App\AffiliatePayment;
use App\Traits\UserTrait;
use PayPal\Rest\ApiContext;
use PayPal\Api\Transaction;
use Illuminate\Http\Request;
use PayPal\Api\RedirectUrls;
use PayPal\Api\PaymentExecution;
use Illuminate\Support\Facades\Hash;
use PayPal\Auth\OAuthTokenCredential;
use Illuminate\Support\Facades\Input;


class PayPalController extends Controller
{

    use UserTrait;

    private $_api_context;

    public function __construct()
    {
        /** PayPal api context **/
        $paypal_conf = \Config::get('paypal');

        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf[$paypal_conf['mode']]['client_id'],
            $paypal_conf[$paypal_conf['mode']]['secret']
        )
        );
        $this->_api_context->setConfig( $paypal_conf[$paypal_conf['mode']]);
    }
    
     /**
     * Display checkout page.
     *
     * @return Response
     */
    public function checkingOut(Request $request)
    { 
        $validatedData = $request->validate([
            'plan' => 'required',
            'name' => 'required|string|max:255|regex:/^[a-zA-Z ]+$/',
            'email' => 'required|string|email|max:255|unique:users',
            'mobile' => 'required|numeric|digits:11',
            'password' => 'required|string|min:6|confirmed',
            'address' => 'required|string|max:255|regex:/^[a-zA-Z ]+$/',
            'country' => 'required|string|max:255|regex:/^[a-zA-Z ]+$/',
            'state' => 'required|string|max:255|regex:/^[a-zA-Z ]+$/',
            'city' => 'required|string|max:255|regex:/^[a-zA-Z ]+$/',
            'zipcode' => 'required|integer|min:0',
        ]);
       $result =  DB::transaction(function()use($validatedData){

	        $userUsedFreePlanBefore = Cookie::get('usedTrialPlan');

	        if(!is_null($userUsedFreePlanBefore) && $validatedData['plan']=='trial'){

	            return redirect('/payment')->withErrors(['plan'=>__('errors.plan')]);  
	          }
	          
	        $cookie='';

	        if(is_null($userUsedFreePlanBefore) && $validatedData['plan']=='trial'){    
	           $cookie = cookie('usedTrialPlan', 'true', 43800);//one month
	        }
	        
	        $user = $this->registerUser($validatedData);

			$url = $this->payWithpaypal($validatedData,$user);
		
			$result = ['url' => $url,'cookie' => $cookie];
			return $result;
        });
       if(!$result['cookie']){
			return redirect($result['url']);
		}
        return redirect($result['url'])->withCookie($result['cookie']);
    }



    public function registerUser($request){

        $expirationDay = Carbon::now();

        if($request['plan'] === 'trial'){
            $expirationDay = $expirationDay->addDays(7);
        }
        else{
            $expirationDay = $expirationDay->addDays(30);
        }

        $affiliateId = $this->generateUserLink();

       return  DB::transaction(function()use($request,$affiliateId,$expirationDay){

            $user = User::create([
            'name' => $request['name'],
            'password' => Hash::make($request['password']),
            'country' => $request['country'],
            'state' => $request['state'],
            'city' => $request['city'],
            'mobile' => $request['mobile'],
            'email' => $request['email'],
            'address' => $request['address'],
            'zipcode' => $request['zipcode'],
            'plan' => $request['plan'],
            'expiration' => $expirationDay,
            'affiliate_id' => $affiliateId,
        ]);
            $verifyUser = VerifyUser::create([
                'user_id' => $user->id,
                'token' => str_random(40)
            ]);

            Mail::to($user->email)->send(new VerifyMail($user));


            $ref = request()->query('ref');
            
            if(!$ref){return $user;}
            $affiliate = User::where('affiliate_id',$ref)->where('verified',1)->first();
            if(!$affiliate){return;}

            $user->referred_by = $affiliate->id;
            $user->save();

            AffiliatePayment::create(['user_id' => $user->id,'affiliate_id'=> $affiliate->id,'paid'=>false]);

            return $user;

        });
    }


 public function payWithpaypal($request,$user)
    {

    	$plan = $request['plan'];
        $total='0';

        if(isset($plan) && !empty($plan)){
            switch ($plan) {
                case "trial":
                    $total='1';
                    break;
                case "basic":
                    $total='69';
                    break;
                case "pro":
                    $total='99';
                    break;
                default:
                    $total='0';
            }
        }

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $item_1 = new Item();
        $item_1->setName('Item 1') /** item name **/
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setPrice($total); /** unit price **/
        $item_list = new ItemList();
        $item_list->setItems(array($item_1));
        $amount = new Amount();
        $amount->setCurrency('USD')
            ->setTotal($total);
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Your transaction description');
        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::to('status')) /** Specify return URL **/
            ->setCancelUrl(URL::to('status'));
        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
        /** dd($payment->create($this->_api_context));exit; **/
        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                \Session::put('error', 'Connection timeout');
                return Redirect::to('/');
            } else {
                \Session::put('error', 'Some error occur, sorry for inconvenient');
                return Redirect::to('/');
            }
        }
        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        $user->paypal_payment_id = $payment->getId();
        $user->save();
        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());
       return $redirect_url;
    }



    public function getPaymentStatus()
    {

        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');

    	$user = User::where('paypal_payment_id',$payment_id)->first();
        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            \Session::put('error', 'Payment failed');
            return Redirect::to('/');
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);
        if ($result->getState() == 'approved') {
            \Session::put('success', 'Payment success');
            $user->paid = 1;
            $user->save();
            $this->payAffiliateBack($user);
            return Redirect::to('/');
        }
        \Session::put('error', 'Payment failed');
        return Redirect::to('/');
    }

   public function payAffiliateBack($user){
  
      $affiliate = User::whereId($user->referred_by)->first();

      if(!$affiliate){return;}

      if($user->affiliate->affiliate_id === $affiliate->id && $user->affiliate->paid === 1)
      {
        return;
      }
      
      $this->payAffiliateByEmail($affiliate,$user);
      return;
    }


  public function payAffiliateByEmail(User $affiliate,User $user){

    $provider = PayPal::setProvider('adaptive_payments');

    $data = [
      'receivers'  => [
        [
        'email' => $affiliate->email,
        'amount' => 1,
        'primary' => false,
        ]
      ],
      'payer' => 'EACHRECEIVER', // (Optional) Describes who pays PayPal fees. Allowed values are: 'SENDER', 'PRIMARYRECEIVER', 'EACHRECEIVER' (Default), 'SECONDARYONLY'
      'return_url' => url('home'),//success  
      'cancel_url' => url('home'),//cancel 
    ];

    $response = $provider->createPayRequest($data);
    $redirect_url = $provider->getRedirectUrl('approved', $response['payKey']);

    Mail::to($affiliate->email)->send(new AffiliateMail($redirect_url));

    if(!count(Mail::failures())){

      $payment = AffiliatePayment::where('affiliate_id',$affiliate->id)
      ->where('user_id',$user->id)
      ->first();
      $payment->paid =true;
      $payment->save();
    
    }
    return $response;
  }

}
