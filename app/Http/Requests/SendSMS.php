<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class SendSMS extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required',
            'mobile' => 'required|numeric',
            'message' => 'required'
        ];
    }

   public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $data = request()->all();  
            if (!$this->userUsedHisMobile($data)) {
                $validator->errors()->add('mobile', 'Please enter a correct number!!');
            }       

            if (!User::where('id',$data['id'])->first()->paid) {
                $validator->errors()->add('mobile', 'Not a piad user!!');
            }
        });
    }

    protected function userUsedHisMobile($data){

        return User::where('mobile',$data['mobile'])
        ->where('id',$data['id'])
        ->exists();
    }

}
