<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Session;
class CheckVerifiedUsers
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(Auth::user()->verified === 1){
            return $next($request);
        }
        Session::flash('confirm_mail', __('errors.mail_confirm'));         
        Auth::logout();
        return redirect('/login'); 
    }
}
