<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SMSPolicy
{
    use HandlesAuthorization;

    /**
     * Determine if the given user can send sms.
     *
     * @param  \App\User  $user
     * @return bool
     */
    public function send(User $user)
    {
        return $user->isAdmin();
    }

}
