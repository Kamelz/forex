<?php
namespace App\Traits;
use App\User;
use App\Payment\PayMobWrapper;

trait UserTrait {
	
	public $payMob;

	public function __construct(){
		$this->payMob = new PayMobWrapper();
	}

    public function generateUserLink(){

        $affiliateLink = str_random(10);
        if(User::where('affiliate_id',$affiliateLink)->count()){
           $this->generateUserLink();
        }
        return $affiliateLink;
    }


    public function getPaymentKey($auth,$total,$paymobOrder,$validatedData){

    	return $this->payMob->getPaymentKeyPaymob( // get payment key
            $auth->token,
            $total,
            $paymobOrder->id,
            // For billing data
            $validatedData['email'], 
            $validatedData['name'], 
            $validatedData['mobile'], 
            $validatedData['city'], 
            $validatedData['country'] 
        );


    }
}
