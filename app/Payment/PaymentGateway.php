<?php
namespace App\Payment;
use App\Payment\Twocheckout\lib\Twocheckout;
use App\Payment\Twocheckout\lib\Twocheckout\Twocheckout_Charge;
use App\Payment\Twocheckout\lib\Twocheckout\Api\Twocheckout_Error;
use PayPal;

class PaymentGateway
{
	public function __construct(){
		Twocheckout::privateKey('683C983E-212D-4A63-912B-FD887809CCB6');
		Twocheckout::sellerId('901384326'); 
		Twocheckout::sandbox(true); //set to fasle in production
	}

	public function charge($request){
		try {

			$plan = $request->query('plan')??null;
			$total='0.0';
			if(isset($plan) && !empty($plan)){
                switch ($plan) {
                    case "trial":
                        $total='1.00';
                        break;
                    case "basic":
                        $total='69.00';
                        break;
                    case "pro":
                        $total='99.0';
                        break;
                    default:
                        $total='0.0';
                }
            }
		
		$charge = Twocheckout_Charge::auth(array(
		"token"      => $request['token'],
		"currency"   => 'USD',
		"total"      => $total,
		"billingAddr" => array(
		"name" => $request['name'],
		"addrLine1" => $request['address'],
		"city" => $request['city'],
		"state" => $request['state'],
		"zipCode" => $request['zipcode'],
		"country" => $request['country'],
		"email" => $request['email'],
		"phoneNumber" => $request['mobile']
		)
		));
		if ($charge['response']['responseCode'] == 'APPROVED') {
			return $charge;
		}
		}catch (Twocheckout_Error $e) {
		print_r($e->getMessage());
		}
	}
	public function payAffiliate()
	{
		PayPal::setProvider('adaptive_payments');

		$data = [
    'receivers'  => [
        [
            'email' => 'mohamed.php@qtmdigital.com',
            'amount' => 10,
            'primary' => true,
        ]
    ],
    'payer' => 'EACHRECEIVER', // (Optional) Describes who pays PayPal fees. Allowed values are: 'SENDER', 'PRIMARYRECEIVER', 'EACHRECEIVER' (Default), 'SECONDARYONLY'
    'return_url' => url('payment/success'), 
    'cancel_url' => url('payment/cancel'),
];

$response = $provider->createPayRequest($data);
	}
}