<?php
namespace App\Payment;

use DB;
use Mail;
use Cookie;
use App\User;
use Carbon\Carbon;
use App\VerifyUser;
use App\Mail\VerifyMail;
use App\Traits\UserTrait;
use App\Mail\AffiliateMail;
use Illuminate\Support\Facades\Hash;

use BaklySystems\PayMob\PayMob;

class PayMobWrapper extends PayMob{
    
    use UserTrait;
    
    protected $payload;

    public function __construct(){

    }

    public function setPayLoad($payload){

        $this->payload = $payload;
    }   

    public function getPayLoad(){

        return $this->payload;
    }

  
    /**
     * Register order to paymob servers
     *
     * @param  string  $token
     * @param  int  $merchant_id
     * @param  int  $amount_cents
     * @param  int  $merchant_order_id
     * @return array
     */
    public function makeOrderPaymob($token, $merchant_id, $amount_cents, $merchant_order_id)
    {
        // Request body
        $json = [
            'merchant_id'            => $merchant_id,
            'amount_cents'           => $amount_cents,
            'merchant_order_id'      => $merchant_order_id,
            'currency'               => 'USD',
            'notify_user_with_email' => false
        ];

        // Send curl
        $order = $this->cURL(
            'https://accept.paymobsolutions.com/api/ecommerce/orders?token='.$token,
            $json
        );
        return $order;
    }

    /**
     * Get payment key to load iframe on paymob servers
     *
     * @param  string  $token
     * @param  int  $amount_cents
     * @param  int  $order_id
     * @param  string  $email
     * @param  string  $fname
     * @param  string  $lname
     * @param  int  $phone
     * @param  string  $city
     * @param  string  $country
     * @return array
     */
    public function getPaymentKeyPaymob(
          $token,
          $amount_cents,
          $order_id,
          $email   = 'null',
          $fname   = 'null',
          $lname   = 'null',
          $phone   = 'null',
          $city    = 'null',
          $country = 'null'
      ) {
        // Request body
        $json = [
            'amount_cents' => $amount_cents,
            'expiration'   => 36000,
            'order_id'     => $order_id,
            "billing_data" => [
                "email"        => $email,
                "first_name"   => $fname,
                "last_name"    => $lname,
                "phone_number" => $phone,
                "city"         => $city,
                "country"      => $country,
                'street'       => 'null',
                'building'     => 'null',
                'floor'        => 'null',
                'apartment'    => 'null'
            ],
            'currency'            => 'USD',
            'card_integration_id' => config('paymob.integration_id')
        ];

        // Send curl
        $payment_key = $this->cURL(
            'https://accept.paymobsolutions.com/api/acceptance/payment_keys?token='.$token,
            $json
        );

        return $payment_key;
    }

    public function getCost(){

        $plan = $this->payload['plan'];
        $total='0';

        if(isset($plan) && !empty($plan)){
            switch ($plan) {
                case "trial":
                    $total='1'*100;
                    break;
                case "basic":
                    $total='69'*100;
                    break;
                case "pro":
                    $total='99'*100;
                    break;
                default:
                    $total='0';
            }
        }
        return $total;
    }

<<<<<<< HEAD
    public function getPaymentUrl($auth){ //todo refactor this
=======
    public function getPaymentUrl($auth){ 
>>>>>>> bbf781e14753a6eaed61d8726a484ea66e8edee6

        $url = DB::transaction(function()use($auth){
       
              $total = $this->getCost();  

              $paymobOrder =  $this->makeOrder($auth,$total);

              $user = $this->registerUser($this->payload);
       
              $user->orders()->create(['paymob_order_id' => $paymobOrder->id]);

              $payment_key = $this->getPaymentKey($auth,$total,$paymobOrder,$this->payload);

               return  "https://accept.paymobsolutions.com/api/acceptance/iframes/".config('paymob.iframe_id')."?payment_token={$payment_key->token}";
          });

        return $url;
    }



    public function registerUser($request){

        $expirationDay = Carbon::now();

        if($request['plan'] === 'trial'){
            $expirationDay = $expirationDay->addDays(7);
        }
        else{
            $expirationDay = $expirationDay->addDays(30);
        }

        $affiliateId = $this->generateUserLink();

       return  DB::transaction(function()use($request,$affiliateId,$expirationDay){

            $user = User::create([
            'name' => $request['name'],
            'password' => Hash::make($request['password']),
            'country' => $request['country'],
            'state' => $request['state'],
            'city' => $request['city'],
            'mobile' => $request['mobile'],
            'email' => $request['email'],
            'address' => $request['address'],
            'zipcode' => $request['zipcode'],
            'plan' => $request['plan'],
            'expiration' => $expirationDay,
            'affiliate_id' => $affiliateId,
        ]);
            $verifyUser = VerifyUser::create([
                'user_id' => $user->id,
                'token' => str_random(40)
            ]);

            Mail::to($user->email)->send(new VerifyMail($user));


            $ref = request()->query('ref');
            
            if(!$ref){return $user;}
            $affiliate = User::where('affiliate_id',$ref)->where('verified',1)->first();
            if(!$affiliate){return;}

            $user->referred_by = $affiliate->id;
            $user->save();

            AffiliatePayment::create(['user_id' => $user->id,'affiliate_id'=> $affiliate->id,'paid'=>false]);

            return $user;

        });
    }

    public function generateMerchantId(){

        return str_random(10);
    }

    public function makeOrder($auth,$total){
           
    $paymobOrder = $this->makeOrderPaymob( // make order on PayMob
      $auth->token,
      $auth->profile->id,
      $total,
      $this->generateMerchantId()
    );

    // Duplicate order id
        // PayMob saves your order id as a unique id as well as their id as a primary key, thus your order id must not
        // duplicate in their database. 
        if (isset($paymobOrder->message)) {
            if ($paymobOrder->message == 'duplicate') {
                $this->makeOrder($auth,$total);
            }
        }

        return $paymobOrder;
    }


}