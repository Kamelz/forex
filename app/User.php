<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'affiliate_id',
        'country',
        'state',
        'city',
        'mobile',
        'address',
        'email',
        'paypal_payment_id',
        'mobile',
        'zipcode',
        'plan',
        'expiration'
        ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['expiration'];

    public function verifyUser(){

        return $this->hasOne('App\VerifyUser');
    }

    public function affiliate(){
     
        return $this->hasOne('App\AffiliatePayment','user_id','id');
    }

    public function referedUsers(){
     
        return $this->hasOne('App\AffiliatePayment','affiliate_id','id');
    }

    public function orders(){

         return $this->hasMany('App\Order');
    }

    public function isAdmin(){
        
        return auth()->user()->is_admin;
    }

}

