@extends('layouts.app')

@section('content')
    <div class="container">
        @include('layouts.sidebar')
        <div class="row justify-content-center">
            @if (session('status'))
                <div style='margin-left: 38%;' class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{__('dashboard.announcements')}}</div>

                    <div class="card-body">
                        <form method="POST" action="/announcements">
                          {{ csrf_field() }}

                            <div class="form-group row">
                                <label for="title" class="col-sm-4 col-form-label text-md-right">{{__('dashboard.title')}}
                                </label>

                                <div class="col-md-6">
                                    <input id="title" type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title') }}"  autofocus>

                                    @if ($errors->has('title'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="body" class="col-sm-4 col-form-label text-md-right">
                                    {{__('dashboard.body')}}
                                </label>

                                <div class="col-md-6">
                                    <textarea id="body" type="text" class="form-control{{ $errors->has('body') ? ' is-invalid' : '' }}" name="body"  autofocus>
                                        {{ old('body') }}
                                    </textarea>

                                    @if ($errors->has('body'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('body') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>



                            <div class="form-group row">
                                <label for="link" class="col-sm-4 col-form-label text-md-right">
                            {{__('dashboard.link')}}

                            </label>

                                <div class="col-md-6">
                                    <input id="link" type="text" class="form-control{{ $errors->has('link') ? ' is-invalid' : '' }}" name="link" value="{{ old('link') }}"  autofocus>

                                    @if ($errors->has('link'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('link') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="note" class="col-sm-4 col-form-label text-md-right">
                                   {{__('dashboard.note')}}
                            </label>

                                <div class="col-md-6">
                                    <input id="note" type="text" class="form-control{{ $errors->has('note') ? ' is-invalid' : '' }}" name="note" value="{{ old('note') }}"  autofocus>

                                    @if ($errors->has('note'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('note') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                     {{__('dashboard.submit')}}
                                    </button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
