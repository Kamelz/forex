@extends('layouts.app')
@section('content')
    <div class="container">
        @include('layouts.sidebar')
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <script type="text/javascript" src="https://widgets.myfxbook.com/scripts/fxCalendar.js?ps=30"></script>
                </div>
            </div>
        </div>
    </div>
@endsection
