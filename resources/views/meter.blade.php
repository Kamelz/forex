@extends('layouts.app')
@section('content')
    <div class="container">
        @include('layouts.sidebar')
        <div class="row">
            <div class="col-xs-12 page-content meter-content">
                <h2 class="head">{{__('sidemenu.meters')}}</h2>
                <div class="row">
                    <div class="col-xs-12 col-sm-6  col-lg-4">
                        <div class="white-box small">
                            <h6 class="head-bk">EUR/USD</h6>
                            <div class="white-box-content" style="width: 100%;">
                                <div class="trade-meter-box1">{{__('dashboard.down')}}</div>
                                <div class="trade-meter-box2"><img class="img-responsive" src="images/new-theme/new/meter2.png" alt="meter"></div>
                                <div class="trade-meter-box3">{{__('dashboard.up')}}</div>
                                <span class="text">{{__('dashboard.update')}}&nbsp;07/02/18  11:10 AM&nbsp;EST</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6  col-lg-4">
                        <div class="white-box small">
                            <h6 class="head-bk">GBP/USD</h6>
                            <div class="white-box-content" style="width: 100%;">
                                <div class="trade-meter-box1">{{__('dashboard.down')}}</div>
                                <div class="trade-meter-box2"><img class="img-responsive" src="images/new-theme/new/meter2.png" alt="meter"></div>
                                <div class="trade-meter-box3">{{__('dashboard.up')}}</div>
                                <span class="text">{{__('dashboard.update')}}&nbsp;07/02/18  11:10 AM&nbsp;EST</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6  col-lg-4">
                        <div class="white-box small">
                            <h6 class="head-bk">USD/CHF</h6>
                            <div class="white-box-content" style="width: 100%;">
                                <div class="trade-meter-box1">{{__('dashboard.down')}}</div>
                                <div class="trade-meter-box2"><img class="img-responsive" src="images/new-theme/new/meter6.png" alt="meter"></div>
                                <div class="trade-meter-box3">{{__('dashboard.up')}}</div>
                                <span class="text">{{__('dashboard.update')}}&nbsp;07/02/18  11:10 AM&nbsp;EST</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6  col-lg-4">
                        <div class="white-box small">
                            <h6 class="head-bk">USD/JPY</h6>
                            <div class="white-box-content" style="width: 100%;">
                                <div class="trade-meter-box1">{{__('dashboard.down')}}</div>
                                <div class="trade-meter-box2"><img class="img-responsive" src="images/new-theme/new/meter5.png" alt="meter"></div>
                                <div class="trade-meter-box3">{{__('dashboard.up')}}</div>
                                <span class="text">{{__('dashboard.update')}}&nbsp;07/02/18  11:10 AM&nbsp;EST</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6  col-lg-4">
                        <div class="white-box small">
                            <h6 class="head-bk">AUD/USD</h6>
                            <div class="white-box-content" style="width: 100%;">
                                <div class="trade-meter-box1">{{__('dashboard.down')}}</div>
                                <div class="trade-meter-box2"><img class="img-responsive" src="images/new-theme/new/meter1.png" alt="meter"></div>
                                <div class="trade-meter-box3">{{__('dashboard.up')}}</div>
                                <span class="text">{{__('dashboard.update')}}&nbsp;07/02/18  11:10 AM&nbsp;EST</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6  col-lg-4">
                        <div class="white-box small">
                            <h6 class="head-bk">EUR/JPY</h6>
                            <div class="white-box-content" style="width: 100%;">
                                <div class="trade-meter-box1">{{__('dashboard.down')}}</div>
                                <div class="trade-meter-box2"><img class="img-responsive" src="images/new-theme/new/meter4.png" alt="meter"></div>
                                <div class="trade-meter-box3">{{__('dashboard.up')}}</div>
                                <span class="text">{{__('dashboard.update')}}&nbsp;07/02/18  11:10 AM&nbsp;EST</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6  col-lg-4">
                        <div class="white-box small">
                            <h6 class="head-bk">USD/CAD</h6>
                            <div class="white-box-content" style="width: 100%;">
                                <div class="trade-meter-box1">{{__('dashboard.down')}}</div>
                                <div class="trade-meter-box2"><img class="img-responsive" src="images/new-theme/new/meter7.png" alt="meter"></div>
                                <div class="trade-meter-box3">{{__('dashboard.up')}}</div>
                                <span class="text">{{__('dashboard.update')}}&nbsp;07/02/18  11:10 AM&nbsp;EST</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6  col-lg-4">
                        <div class="white-box small">
                            <h6 class="head-bk">GBP/JPY</h6>
                            <div class="white-box-content" style="width: 100%;">
                                <div class="trade-meter-box1">{{__('dashboard.down')}}</div>
                                <div class="trade-meter-box2"><img class="img-responsive" src="images/new-theme/new/meter3.png" alt="meter"></div>
                                <div class="trade-meter-box3">{{__('dashboard.up')}}</div>
                                <span class="text">{{__('dashboard.update')}}&nbsp;07/02/18  11:10 AM&nbsp;EST</span>
                            </div>
                        </div>
                    </div>
                    <!--<div class="col-xs-12 col-sm-6  col-lg-3">
                <div class="white-box small">
                    <h6 class="head-bk">GBP/USD</h6>
                    <div class="white-box-content">
                        <img class="img-responsive" src="images/new-theme/meter2.png" alt="meter" />
                        <span class="text">{{__('dashboard.update')}}   08/05/15 06:55 AM EST</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6  col-lg-3">
                <div class="white-box small">
                    <h6 class="head-bk">USD/CHF</h6>
                    <div class="white-box-content">
                        <img class="img-responsive" src="images/new-theme/meter3.png" alt="meter" />
                        <span class="text">{{__('dashboard.update')}}   08/05/15 06:55 AM EST</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6  col-lg-3">
                <div class="white-box small">
                    <h6 class="head-bk">USD/JPY</h6>
                    <div class="white-box-content">
                        <img class="img-responsive" src="images/new-theme/meter2.png" alt="meter" />
                        <span class="text">{{__('dashboard.update')}}   08/05/15 06:55 AM EST</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6  col-lg-3">
                <div class="white-box small">
                    <h6 class="head-bk">GBP/JPY</h6>
                    <div class="white-box-content">
                        <img class="img-responsive" src="images/new-theme/meter5.png" alt="meter" />
                        <span class="text">{{__('dashboard.update')}}   08/05/15 06:55 AM EST</span>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6  col-lg-3">
                <div class="white-box small">
                    <h6 class="head-bk">GBP/JPY</h6>
                    <div class="white-box-content">
                        <img class="img-responsive" src="images/new-theme/meter6.png" alt="meter" />
                        <span class="text">{{__('dashboard.update')}}   08/05/15 06:55 AM EST</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6  col-lg-3">
                <div class="white-box small">
                    <h6 class="head-bk">GBP/JPY</h6>
                    <div class="white-box-content">
                        <img class="img-responsive" src="images/new-theme/meter5.png" alt="meter" />
                        <span class="text">{{__('dashboard.update')}}   08/05/15 06:55 AM EST</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6  col-lg-3">
                <div class="white-box small">
                    <h6 class="head-bk">GBP/JPY</h6>
                    <div class="white-box-content">
                        <img class="img-responsive" src="images/new-theme/meter8.png" alt="meter" />
                        <span class="text">{{__('dashboard.update')}}   08/05/15 06:55 AM EST</span>
                    </div>
                </div>
            </div>-->
                </div>

            </div>
        </div>
    </div>
@endsection
