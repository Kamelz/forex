@extends('layouts.app')
@section('content')
    <div class="container">
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Cost</th>
      <th scope="col">Source</th>
      <th scope="col">Status</th>
      <th scope="col">Date</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>{{$transaction->id}}</td>
      <td>{{$transaction->amount_cents/100}}/{{$transaction->currency}}</td>
      <td>
      	<span>**** **** **** {{$transaction->source_data->pan}}</span>
      </td>
      
      <td>
      	@if($transaction->success)
      		<span style="color:green;">Successful</span>
      	@else
      		<span style="color:red;">Declined</span>
      	@endif
      </td>

      <td>{{Carbon\Carbon::createFromTimeString($transaction->created_at)->format('m/d/Y')}}</td>

    </tr>
  </tbody>
</table>

<div class="row">
  <div class="col-xs-12">
      <!-- todo partners -->
  </div>
</div>
    </div>
@endsection
