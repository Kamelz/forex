@extends('layouts.app')
@section('content')
    <div class="container">
        @include('layouts.sidebar')
        <div class="row">
            <div class="col-xs-12 page-content">

                <form name="frmCustomerEdit" action="/settings" method="post">

                    <div class="white-box email-content">
                        <h6 class="head">Email Alert Settings</h6>
                        <div class="row section">
                            <div class="col-xs-12  col-sm-6 col-lg-4">
                                <label class="label" for="emial1">E-Mail 1</label>
                                <div class="row">
                                    <div class="col-xs-8 input-div">
                                        <input class="text" type="text" name="txtemail1" id="txtemail1" value="" maxlength="100" placeholder="">
                                    </div>
                                    <div class="col-xs-4">
                                        <a class="btn gray" href="#" onclick="removetxtemail1();">Remove</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12  col-sm-6 col-lg-4">
                                <label class="label" for="emial2">E-Mail 2</label>
                                <div class="row">
                                    <div class="col-xs-8 input-div">
                                        <input class="text" type="text" name="txtemail2" id="txtemail2" value="" maxlength="100" placeholder="">
                                    </div>
                                    <div class="col-xs-4">
                                        <a class="btn gray" href="#" onclick="removetxtemail2();">Remove</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12  col-sm-6 col-lg-4">
                                <label class="label" for="emial1">E-Mail 3</label>
                                <div class="row">
                                    <div class="col-xs-8 input-div">
                                        <input class="text" type="text" name="txtemail3" id="txtemail3" value="" maxlength="100" placeholder="">
                                    </div>
                                    <div class="col-xs-4">
                                        <a class="btn gray" href="#" onclick="removetxtemail3();">Remove</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h6 class="head">Mobile/SMS Settings</h6>
                        <div class="row section">
                            <!--<span class="text bold col-xs-12">Mobile/Cell Phone #1 (optional)</span>-->
                            <div class="col-xs-12 col-md-12">
                                <div class="row section">
                                    <div class="col-xs-12  col-sm-6 col-lg-4 input-div">
                                        <label class="label">Country</label>
                                        <select class="country-bottom custom-select bs-select-hidden" name="country1">
                                            <option value="">--Choose Country--</option>
                                            <option value="1">Afghanistan</option>
                                            <option value="2">Albania</option>
                                            <option value="3">Algeria</option>
                                            <option value="4">American Samoa</option>
                                            <option value="5">Andorra</option>
                                            <option value="6">Angola</option>
                                            <option value="7">Anguilla</option>
                                            <option value="9">Antigua &amp; Barbuda</option>
                                            <option value="10">Argentina</option>
                                            <option value="11">Armenia</option>
                                            <option value="12">Aruba</option>
                                            <option value="13">Australia</option>
                                            <option value="14">Austria</option>
                                            <option value="15">Azerbaijan</option>
                                            <option value="16">Bahamas, the</option>
                                            <option value="17">Bahrain</option>
                                            <option value="18">Bangladesh</option>
                                            <option value="19">Barbados</option>
                                            <option value="20">Belarus</option>
                                            <option value="21">Belgium</option>
                                            <option value="22">Belize</option>
                                            <option value="23">Benin</option>
                                            <option value="24">Bermuda</option>
                                            <option value="25">Bhutan</option>
                                            <option value="26">Bolivia</option>
                                            <option value="27">Bosnia and Herzegowina</option>
                                            <option value="28">Botswana</option>
                                            <option value="30">Brazil</option>
                                            <option value="32">Brunei Darussalam</option>
                                            <option value="33">Bulgaria</option>
                                            <option value="34">Burkina Faso</option>
                                            <option value="35">Burundi</option>
                                            <option value="36">Cambodia</option>
                                            <option value="37">Cameroon</option>
                                            <option value="38">Canada</option>
                                            <option value="39">Cape Verde</option>
                                            <option value="40">Cayman Islands</option>
                                            <option value="41">Central African Republic</option>
                                            <option value="42">Chad</option>
                                            <option value="43">Chile</option>
                                            <option value="44">China</option>
                                            <option value="47">Colombia</option>
                                            <option value="49">Congo</option>
                                            <option value="50">Cook Islands</option>
                                            <option value="51">Costa Rica</option>
                                            <option value="53">Croatia</option>
                                            <option value="54">Cuba</option>
                                            <option value="55">Cyprus</option>
                                            <option value="56">Czech Republic</option>
                                            <option value="57">Denmark</option>
                                            <option value="58">Djibouti</option>
                                            <option value="59">Dominica</option>
                                            <option value="60">Dominican Republic</option>
                                            <option value="61">East Timor</option>
                                            <option value="62">Ecuador</option>
                                            <option value="63">Egypt</option>
                                            <option value="64">El Salvador</option>
                                            <option value="65">Equatorial Guinea</option>
                                            <option value="67">Estonia</option>
                                            <option value="68">Ethiopia</option>
                                            <option value="69">Falkland Islands (Malvinas)</option>
                                            <option value="70">Faroe Islands</option>
                                            <option value="71">Fiji</option>
                                            <option value="72">Finland</option>
                                            <option value="73">France</option>
                                            <option value="75">French Guiana</option>
                                            <option value="76">French Polynesia</option>
                                            <option value="78">Gabon</option>
                                            <option value="79">Gambia</option>
                                            <option value="80">Georgia</option>
                                            <option value="81">Germany</option>
                                            <option value="82">Ghana</option>
                                            <option value="83">Gibraltar</option>
                                            <option value="84">Greece</option>
                                            <option value="85">Greenland</option>
                                            <option value="86">Grenada</option>
                                            <option value="87">Guadeloupe</option>
                                            <option value="88">Guam</option>
                                            <option value="89">Guatemala</option>
                                            <option value="90">Guinea</option>
                                            <option value="92">Guyana</option>
                                            <option value="93">Haiti</option>
                                            <option value="95">Honduras</option>
                                            <option value="96">Hong Kong</option>
                                            <option value="97">Hungary</option>
                                            <option value="98">Iceland</option>
                                            <option value="99">India</option>
                                            <option value="100">Indonesia</option>
                                            <option value="101">Iran (Islamic Republic of)</option>
                                            <option value="102">Iraq</option>
                                            <option value="103">Ireland</option>
                                            <option value="104">Israel</option>
                                            <option value="105">Italy</option>
                                            <option value="106">Jamaica</option>
                                            <option value="107">Japan</option>
                                            <option value="108">Jordan</option>
                                            <option value="109">Kazakhstan</option>
                                            <option value="110">Kenya</option>
                                            <option value="113">Korea, (South) Republic of</option>
                                            <option value="114">Kuwait</option>
                                            <option value="115">Kyrgyzstan</option>
                                            <option value="116">Lao People's Democratic Republic</option>
                                            <option value="117">Latvia</option>
                                            <option value="118">Lebanon</option>
                                            <option value="119">Lesotho</option>
                                            <option value="120">Liberia</option>
                                            <option value="121">Libyan Arab Jamahiriya</option>
                                            <option value="122">Liechtenstein</option>
                                            <option value="123">Lithuania</option>
                                            <option value="124">Luxembourg</option>
                                            <option value="125">Macau</option>
                                            <option value="126">Macedonia</option>
                                            <option value="127">Madagascar</option>
                                            <option value="128">Malawi</option>
                                            <option value="129">Malaysia</option>
                                            <option value="130">Maldives</option>
                                            <option value="131">Mali</option>
                                            <option value="132">Malta</option>
                                            <option value="134">Martinique</option>
                                            <option value="135">Mauritania</option>
                                            <option value="136">Mauritius</option>
                                            <option value="137">Mayotte</option>
                                            <option value="138">Mexico</option>
                                            <option value="140">Moldova, Republic of</option>
                                            <option value="141">Monaco</option>
                                            <option value="142">Mongolia</option>
                                            <option value="143">Montserrat</option>
                                            <option value="144">Morocco</option>
                                            <option value="145">Mozambique</option>
                                            <option value="146">Myanmar</option>
                                            <option value="147">Namibia</option>
                                            <option value="149">Nepal</option>
                                            <option value="150">Netherlands</option>
                                            <option value="151">Netherlands Antilles</option>
                                            <option value="152">New Caledonia</option>
                                            <option value="153">New Zealand</option>
                                            <option value="154">Nicaragua</option>
                                            <option value="155">Niger</option>
                                            <option value="156">Nigeria</option>
                                            <option value="160">Norway</option>
                                            <option value="162">Oman</option>
                                            <option value="163">Pakistan</option>
                                            <option value="165">Panama</option>
                                            <option value="166">Papua New Guinea</option>
                                            <option value="167">Paraguay</option>
                                            <option value="168">Peru</option>
                                            <option value="169">Philippines</option>
                                            <option value="171">Poland</option>
                                            <option value="172">Portugal</option>
                                            <option value="173">Puerto Rico</option>
                                            <option value="174">Qatar</option>
                                            <option value="175">Reunion</option>
                                            <option value="176">Romania</option>
                                            <option value="177">Russian Federation</option>
                                            <option value="178">Rwanda</option>
                                            <option value="179">Saint Kitts and Nevis</option>
                                            <option value="180">Saint Lucia</option>
                                            <option value="181">Saint Vincent and The Grenadines</option>
                                            <option value="183">San Marino</option>
                                            <option value="185">Saudi Arabia</option>
                                            <option value="186">Senegal</option>
                                            <option value="187">Seychelles</option>
                                            <option value="188">Sierra Leone</option>
                                            <option value="189">Singapore</option>
                                            <option value="190">Slovakia (Slovak Republic)</option>
                                            <option value="191">Slovenia</option>
                                            <option value="193">Somalia</option>
                                            <option value="194">South Africa</option>
                                            <option value="195">Spain</option>
                                            <option value="196">Sri Lanka</option>
                                            <option value="199">Sudan</option>
                                            <option value="200">Suriname</option>
                                            <option value="202">Swaziland</option>
                                            <option value="203">Sweden</option>
                                            <option value="204">Switzerland</option>
                                            <option value="205">Syrian Arab Republic</option>
                                            <option value="206">Taiwan</option>
                                            <option value="207">Tajikistan</option>
                                            <option value="208">Tanzania, United Republic of</option>
                                            <option value="209">Thailand</option>
                                            <option value="210">Togo</option>
                                            <option value="212">Tonga</option>
                                            <option value="213">Trinidad and Tobago</option>
                                            <option value="214">Tunisia</option>
                                            <option value="215">Turkey</option>
                                            <option value="216">Turkmenistan</option>
                                            <option value="217">Turks and Caicos Islands</option>
                                            <option value="219">Uganda</option>
                                            <option value="220">Ukraine</option>
                                            <option value="221">United Arab Emirates</option>
                                            <option value="222">United Kingdom</option>
                                            <option value="223">United States</option>
                                            <option value="225">Uruguay</option>
                                            <option value="226">Uzbekistan</option>
                                            <option value="227">Vanuatu</option>
                                            <option value="228">Vatican City</option>
                                            <option value="229">Venezuela</option>
                                            <option value="230">Viet Nam</option>
                                            <option value="235">Yemen</option>
                                            <option value="238">Zambia</option>
                                            <option value="239">Zimbabwe</option>
                                            <option value="240">TEMP</option>
                                            <option value="241">Serbia</option>
                                            <option value="242">Montenegro</option>
                                            <option value="243">Jersey</option>
                                            <option value="244">Ivory Coast</option>
                                            <option value="245">Isle of Man</option>
                                            <option value="246">Guernsey and Alderney</option>
                                            <option value="247">Congo, Democratic Republic</option>
                                            <option value="248">British Virgin Islands
                                            </option>
                                        </select>

                                    </div>

                                        <div class="col-xs-12  col-sm-6 col-lg-4 input-div">
                                            <label class="label">Phone Number</label>
                                            <input class="text" id="carrier" type="text" name="mobile" placeholder="" />
                                        </div>

                                </div>
                            </div>
                            <div class="button-set col-xs-12">
                                <a class="btn gray" href="#" onclick="clearmobilephone1()">Remove/Clear Mobile Settings</a>
                            </div>
                        </div>
                        <h6 class="head">Send me alerts at the following times</h6>
                        <div class="row section">
                            <div class="col-xs-10 radio-group">
                                <div class="radio">
                                    <input id="radio1" class="form-check-input"  type="radio" name="alertoption" value="1" onclick="CheckOther();" checked="checked">
                                    <label for="radio1">24 hours a day</label>
                                </div>
                                <div class="radio">
                                    <input id="radio2" class="form-check-input"  type="radio" name="alertoption" value="2" onclick="CheckSpeci();">
                                    <label for="radio2">Between specific times only</label>
                                </div>
                                <div class="radio">
                                    <input id="radio3" class="form-check-input"  type="radio" name="alertoption" value="3" onclick="CheckOther();">
                                    <label for="radio3">Turn off all alerts</label>
                                </div>
                            </div>

                            <div class="col-xs-8" id="spec_time" style="display:none;">
                                <p>All times are Eastern Standard Time / New York Time.</p>
                                <div class="col-xs-12">
                                    <div class="col-xs-3 no-padd">Start receiving alerts at:</div>
                                    <div class="col-xs-1 set_time_cls"><select name="str_time_hour" class="input">
                                            <option value="">HH</option>
                                            <option value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                        </select></div>
                                    <div class="col-xs-1 set_time_cls"><select name="str_time_min" class="input">
                                            <option value="">MM</option>
                                            <option value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                            <option value="14">14</option>
                                            <option value="15">15</option>
                                            <option value="16">16</option>
                                            <option value="17">17</option>
                                            <option value="18">18</option>
                                            <option value="19">19</option>
                                            <option value="20">20</option>
                                            <option value="21">21</option>
                                            <option value="22">22</option>
                                            <option value="23">23</option>
                                            <option value="24">24</option>
                                            <option value="25">25</option>
                                            <option value="26">26</option>
                                            <option value="27">27</option>
                                            <option value="28">28</option>
                                            <option value="29">29</option>
                                            <option value="30">30</option>
                                            <option value="31">31</option>
                                            <option value="32">32</option>
                                            <option value="33">33</option>
                                            <option value="34">34</option>
                                            <option value="35">35</option>
                                            <option value="36">36</option>
                                            <option value="37">37</option>
                                            <option value="38">38</option>
                                            <option value="39">39</option>
                                            <option value="40">40</option>
                                            <option value="41">41</option>
                                            <option value="42">42</option>
                                            <option value="43">43</option>
                                            <option value="44">44</option>
                                            <option value="45">45</option>
                                            <option value="46">46</option>
                                            <option value="47">47</option>
                                            <option value="48">48</option>
                                            <option value="49">49</option>
                                            <option value="50">50</option>
                                            <option value="51">51</option>
                                            <option value="52">52</option>
                                            <option value="53">53</option>
                                            <option value="54">54</option>
                                            <option value="55">55</option>
                                            <option value="56">56</option>
                                            <option value="57">57</option>
                                            <option value="58">58</option>
                                            <option value="59">59</option>
                                        </select></div>
                                    <div class="col-xs-1 set_time_cls"><select name="str_time_format" class="input">
                                            <option value="AM">AM</option>
                                            <option value="PM" selected="">PM</option>
                                        </select></div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="col-xs-3 no-padd">Stop receiving alerts at:</div>
                                    <div class="col-xs-1 set_time_cls"><select name="stop_time_hour" class="input">
                                            <option value="">HH</option>
                                            <option value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                        </select></div>
                                    <div class="col-xs-1 set_time_cls"><select name="stop_time_min" class="input">
                                            <option value="">MM</option>
                                            <option value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                            <option value="14">14</option>
                                            <option value="15">15</option>
                                            <option value="16">16</option>
                                            <option value="17">17</option>
                                            <option value="18">18</option>
                                            <option value="19">19</option>
                                            <option value="20">20</option>
                                            <option value="21">21</option>
                                            <option value="22">22</option>
                                            <option value="23">23</option>
                                            <option value="24">24</option>
                                            <option value="25">25</option>
                                            <option value="26">26</option>
                                            <option value="27">27</option>
                                            <option value="28">28</option>
                                            <option value="29">29</option>
                                            <option value="30">30</option>
                                            <option value="31">31</option>
                                            <option value="32">32</option>
                                            <option value="33">33</option>
                                            <option value="34">34</option>
                                            <option value="35">35</option>
                                            <option value="36">36</option>
                                            <option value="37">37</option>
                                            <option value="38">38</option>
                                            <option value="39">39</option>
                                            <option value="40">40</option>
                                            <option value="41">41</option>
                                            <option value="42">42</option>
                                            <option value="43">43</option>
                                            <option value="44">44</option>
                                            <option value="45">45</option>
                                            <option value="46">46</option>
                                            <option value="47">47</option>
                                            <option value="48">48</option>
                                            <option value="49">49</option>
                                            <option value="50">50</option>
                                            <option value="51">51</option>
                                            <option value="52">52</option>
                                            <option value="53">53</option>
                                            <option value="54">54</option>
                                            <option value="55">55</option>
                                            <option value="56">56</option>
                                            <option value="57">57</option>
                                            <option value="58">58</option>
                                            <option value="59">59</option>
                                        </select></div>
                                    <div class="col-xs-1 set_time_cls"><select name="stop_time_format" class="input">
                                            <option value="AM">AM</option>
                                            <option value="PM" selected="">PM</option>
                                        </select></div>
                                </div>
                            </div>


                        </div>
                        <h6 class="head">I want to receive alerts for the following currencies</h6>
                        <div class="row ">
                            <div class="col-xs-8 form-check">
                                <div class="col-xs-8 checkbox-group">
                                    <div class="clearfix">
                                        <div class="checkbox col-xs-12 sendme">
                                            <input id="chkentpoint" name="chkentpoint" class="styled" type="checkbox" onclick="chkallentpoint()" value="1">
                                            <label for="chkentpoint">Check All</label>
                                        </div>
                                    </div><div class="clearfix"><div class="checkbox col-xs-6 col-md-3">
                                            <input id="entpoint[0]" name="entpoint[]"  class="styled" type="checkbox" value="9">
                                            <label for="entpoint[0]">EUR/USD</label>
                                        </div><div class="checkbox col-xs-6 col-md-3">
                                            <input id="entpoint[1]" name="entpoint[]"  class="styled" type="checkbox" value="8">
                                            <label for="entpoint[1]">GBP/USD</label>
                                        </div><div class="checkbox col-xs-6 col-md-3">
                                            <input id="entpoint[2]" name="entpoint[]"  class="styled" type="checkbox" value="14">
                                            <label for="entpoint[2]">USD/CHF</label>
                                        </div><div class="checkbox col-xs-6 col-md-3">
                                            <input id="entpoint[3]" name="entpoint[]"  class="styled" type="checkbox" value="15">
                                            <label for="entpoint[3]">USD/JPY</label>
                                        </div></div><div class="clearfix"><div class="checkbox col-xs-6 col-md-3">
                                            <input id="entpoint[4]" name="entpoint[]"  class="styled" type="checkbox" value="17">
                                            <label for="entpoint[4]">AUD/USD</label>
                                        </div><div class="checkbox col-xs-6 col-md-3">
                                            <input id="entpoint[5]" name="entpoint[]"  class="styled" type="checkbox" value="13">
                                            <label for="entpoint[5]">EUR/JPY</label>
                                        </div><div class="checkbox col-xs-6 col-md-3">
                                            <input id="entpoint[6]" name="entpoint[]"  class="styled" type="checkbox" value="16">
                                            <label for="entpoint[6]">USD/CAD</label>
                                        </div><div class="checkbox col-xs-6 col-md-3">
                                            <input id="entpoint[7]" name="entpoint[]"  class="styled" type="checkbox" value="22">
                                            <label for="entpoint[7]">GBP/JPY</label>
                                        </div></div><div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <input type="hidden" name="currency_count" id="currency_count" value="8">
                    <div class="page-bottom-text">
                        <input type="submit" class="btn green" style="color:white;" value="Save Settings">
                    </div>
                </form>
            </div>
        </div>

    </div>
@endsection
