@extends('layouts.app')
@yield('layouts.sidebar')
@section('content')
    <div class="container">
        @include('layouts.sidebar')
        <div class="row">
            <div class="alert alert-success" style='margin-left: 38%;display: none' id='success' role="alert">

            </div>

            <div class="alert alert-danger" style='margin-left: 38%; display: none' id='error' role="alert">

            </div>

            @if (session('status'))
                <div style='margin-left: 38%;' class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif


        </div>
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h2 class="head">{{__('dashboard.send-sms')}}</h2>
                <div class="white-box">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__('dashboard.name')}}</th>
                            <th>{{__('dashboard.email')}}</th>
                            <th>{{__('dashboard.number')}}</th>
                            <th>{{__('dashboard.message')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{$user->id}}</td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->mobile}}</td>
                                <td>
                                    <form>
                                        {{ csrf_field() }}
                                        <input type="hidden" name="mobile" value="{{$user->mobile}}">
                                        <input type="hidden" name="id" value="{{$user->id}}">
                                        <div class="form-group">
                                            <textarea class='form-control' name='message'></textarea>
                                        </div>
                                        <button onclick="sendSMS(this)" type="button" class="btn btn-default">{{__('dashboard.send')}}</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">


            <div class="col-md-12">

                <div class="white-box">
                    <h2 class="head">{{__('dashboard.send-to-all')}}</h2>
                    <form method='POST' action="/sms">
                        <div class="form-group">
                          {{ csrf_field() }}
                            <textarea class='form-control' style="width:50%" name='message'></textarea>
                        </div>
                                  @foreach ($errors->all() as $error)
                <div  style='width: 26%;' class="alert alert-danger">
                    {{$error}}
                </div>
            @endforeach

                        <button type="submit" class="btn btn-default">{{__('dashboard.send')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection