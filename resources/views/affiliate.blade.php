@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @include('layouts.sidebar')
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{__('dashboard.affiliate-link')}}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if(Auth::user()->affiliate_id)
                            <input type="text" style='width:100%' readonly="readonly"
                                   value="{{'http://www.forexsignaltrade.com/pricing-and-signup/?ref='.Auth::user()->affiliate_id}}">
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
