@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-5"></div>
        <div class="col-md-7">
            <h2 class="head">{{__('dashboard.welcome')}}</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="white-box">
                <span class="roomaccess" style="color:blue;font-size:large;">{{__('dashboard.announcements')}}</span>
                <br>
                <br>
                <h3><p><font size="4"></font></p>
                    <p><font size="4"><font color="#ff0000">{{$announcements->title??''}}</font><br>
                        </font></p>
                    <p><font size="4">&nbsp;</font></p>
                    <p><font size="4">{{$announcements->body??__('dashboard.no-announcements')}}&nbsp;</font></p>
                        @if(isset($announcements->link))
                        <div><a target="_blank" href="{{$announcements->link}}"><strong><font color="#0033cc">Learn more</font></strong></a></div>
                        @endif
                    <div><font face="Verdana"><br>
                        </font></div>
                    <div><font face="Verdana">{{$announcements->note??''}}</font></div>
                    <div>&nbsp;</div></h3>
            </div>
        </div>

    </div>
    <div class="row">

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="white-box small">
                    <h6 class="head-bk "><span class="round red"></span>EUR/USD<a class="icon-graph" onclick="open_graph_popup('Jmx0O3NjcmlwdCB0eXBlPSZxdW90O3RleHQvamF2YXNjcmlwdCZxdW90OyZndDsNCiAgICBuZXcgVHJhZGluZ1ZpZXcud2lkZ2V0KHsNCiAgICAgJnF1b3Q7Y29udGFpbmVyX2lkJnF1b3Q7OiZxdW90O0VVUlVTRCZxdW90OywNCiAgICAgICZxdW90O2F1dG9zaXplJnF1b3Q7OiB0cnVlLA0KICAgICAgJnF1b3Q7c3ltYm9sJnF1b3Q7OiAmcXVvdDtGWDpFVVJVU0QmcXVvdDssDQogICAgICAmcXVvdDtpbnRlcnZhbCZxdW90OzogJnF1b3Q7NjAmcXVvdDssDQogICAgICAmcXVvdDt0aW1lem9uZSZxdW90OzogJnF1b3Q7RXRjL1VUQyZxdW90OywNCiAgICAgICZxdW90O3RoZW1lJnF1b3Q7OiAmcXVvdDtXaGl0ZSZxdW90OywNCiAgICAgICZxdW90O3N0eWxlJnF1b3Q7OiAmcXVvdDsxJnF1b3Q7LA0KICAgICAgJnF1b3Q7dG9vbGJhcl9iZyZxdW90OzogJnF1b3Q7I2YxZjNmNiZxdW90OywNCiAgICAgICZxdW90O2FsbG93X3N5bWJvbF9jaGFuZ2UmcXVvdDs6IHRydWUsDQogICAgICAmcXVvdDtoaWRlaWRlYXMmcXVvdDs6IHRydWUNCiAgICB9KTsNCiZsdDsvc2NyaXB0Jmd0Ow==');"></a></h6>
                    <div class="white-box-content">
                        <h6 class="head">{{__('dashboard.trade-status')}}</h6>
                        <span class="text">{{__('dashboard.waiting')}}</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="white-box small">
                    <h6 class="head-bk "><span class="round red"></span>GBP/USD<a class="icon-graph" onclick="open_graph_popup('Jmx0O3NjcmlwdCB0eXBlPSZxdW90O3RleHQvamF2YXNjcmlwdCZxdW90OyZndDsNCiAgICBuZXcgVHJhZGluZ1ZpZXcud2lkZ2V0KHsNCiAgICAgICZxdW90O2NvbnRhaW5lcl9pZCZxdW90OzomcXVvdDtHQlBVU0QmcXVvdDssDQogICAgICAmcXVvdDthdXRvc2l6ZSZxdW90OzogdHJ1ZSwNCiAgICAgICZxdW90O3N5bWJvbCZxdW90OzogJnF1b3Q7Rlg6R0JQVVNEJnF1b3Q7LA0KICAgICAgJnF1b3Q7aW50ZXJ2YWwmcXVvdDs6ICZxdW90OzYwJnF1b3Q7LA0KICAgICAgJnF1b3Q7dGltZXpvbmUmcXVvdDs6ICZxdW90O0V0Yy9VVEMmcXVvdDssDQogICAgICAmcXVvdDt0aGVtZSZxdW90OzogJnF1b3Q7V2hpdGUmcXVvdDssDQogICAgICAmcXVvdDtzdHlsZSZxdW90OzogJnF1b3Q7MSZxdW90OywNCiAgICAgICZxdW90O3Rvb2xiYXJfYmcmcXVvdDs6ICZxdW90OyNmMWYzZjYmcXVvdDssDQogICAgICAmcXVvdDthbGxvd19zeW1ib2xfY2hhbmdlJnF1b3Q7OiB0cnVlLA0KICAgICAgJnF1b3Q7aGlkZWlkZWFzJnF1b3Q7OiB0cnVlDQogICAgfSk7DQombHQ7L3NjcmlwdCZndDs=');"></a></h6>
                    <div class="white-box-content">
                        <h6 class="head">{{__('dashboard.trade-status')}}</h6>
                        <span class="text">{{__('dashboard.waiting')}}</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="white-box small">
                    <h6 class="head-bk "><span class="round red"></span>USD/CHF<a class="icon-graph" onclick="open_graph_popup('Jmx0O3NjcmlwdCB0eXBlPSZxdW90O3RleHQvamF2YXNjcmlwdCZxdW90OyZndDsNCiAgICBuZXcgVHJhZGluZ1ZpZXcud2lkZ2V0KHsNCiZxdW90O2NvbnRhaW5lcl9pZCZxdW90OzomcXVvdDtVU0RDSEYmcXVvdDssDQogICAgICAmcXVvdDthdXRvc2l6ZSZxdW90OzogdHJ1ZSwNCiAgICAgICZxdW90O3N5bWJvbCZxdW90OzogJnF1b3Q7Rlg6VVNEQ0hGJnF1b3Q7LA0KICAgICAgJnF1b3Q7aW50ZXJ2YWwmcXVvdDs6ICZxdW90OzYwJnF1b3Q7LA0KICAgICAgJnF1b3Q7dGltZXpvbmUmcXVvdDs6ICZxdW90O0V0Yy9VVEMmcXVvdDssDQogICAgICAmcXVvdDt0aGVtZSZxdW90OzogJnF1b3Q7V2hpdGUmcXVvdDssDQogICAgICAmcXVvdDtzdHlsZSZxdW90OzogJnF1b3Q7MSZxdW90OywNCiAgICAgICZxdW90O3Rvb2xiYXJfYmcmcXVvdDs6ICZxdW90OyNmMWYzZjYmcXVvdDssDQogICAgICAmcXVvdDthbGxvd19zeW1ib2xfY2hhbmdlJnF1b3Q7OiB0cnVlLA0KICAgICAgJnF1b3Q7aGlkZWlkZWFzJnF1b3Q7OiB0cnVlDQogICAgfSk7DQombHQ7L3NjcmlwdCZndDs=');"></a></h6>
                    <div class="white-box-content">
                        <h6 class="head">{{__('dashboard.trade-status')}}</h6>
                        <span class="text">{{__('dashboard.waiting')}}</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="white-box small">
                    <h6 class="head-bk "><span class="round red"></span>USD/JPY<a class="icon-graph" onclick="open_graph_popup('Jmx0O3NjcmlwdCB0eXBlPSZxdW90O3RleHQvamF2YXNjcmlwdCZxdW90OyZndDsNCiAgICBuZXcgVHJhZGluZ1ZpZXcud2lkZ2V0KHsNCiZxdW90O2NvbnRhaW5lcl9pZCZxdW90OzomcXVvdDtVU0RKUFkmcXVvdDssDQogICAgICAmcXVvdDthdXRvc2l6ZSZxdW90OzogdHJ1ZSwNCiAgICAgICZxdW90O3N5bWJvbCZxdW90OzogJnF1b3Q7Rlg6VVNESlBZJnF1b3Q7LA0KICAgICAgJnF1b3Q7aW50ZXJ2YWwmcXVvdDs6ICZxdW90OzYwJnF1b3Q7LA0KICAgICAgJnF1b3Q7dGltZXpvbmUmcXVvdDs6ICZxdW90O0V0Yy9VVEMmcXVvdDssDQogICAgICAmcXVvdDt0aGVtZSZxdW90OzogJnF1b3Q7V2hpdGUmcXVvdDssDQogICAgICAmcXVvdDtzdHlsZSZxdW90OzogJnF1b3Q7MSZxdW90OywNCiAgICAgICZxdW90O3Rvb2xiYXJfYmcmcXVvdDs6ICZxdW90OyNmMWYzZjYmcXVvdDssDQogICAgICAmcXVvdDthbGxvd19zeW1ib2xfY2hhbmdlJnF1b3Q7OiB0cnVlLA0KICAgICAgJnF1b3Q7aGlkZWlkZWFzJnF1b3Q7OiB0cnVlDQogICAgfSk7DQombHQ7L3NjcmlwdCZndDs=');"></a></h6>
                    <div class="white-box-content">
                        <h6 class="head">{{__('dashboard.trade-status')}}</h6>
                        <span class="text">{{__('dashboard.waiting')}}</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="white-box small">
                    <h6 class="head-bk "><span class="round red"></span>AUD/USD<a class="icon-graph" onclick="open_graph_popup('Jmx0O3NjcmlwdCB0eXBlPSZxdW90O3RleHQvamF2YXNjcmlwdCZxdW90OyZndDsNCiAgICBuZXcgVHJhZGluZ1ZpZXcud2lkZ2V0KHsNCiAgICAgICZxdW90O2NvbnRhaW5lcl9pZCZxdW90OzomcXVvdDtBVURVU0QmcXVvdDssDQogICAgICAmcXVvdDthdXRvc2l6ZSZxdW90OzogdHJ1ZSwNCiAgICAgICZxdW90O3N5bWJvbCZxdW90OzogJnF1b3Q7Rlg6QVVEVVNEJnF1b3Q7LA0KICAgICAgJnF1b3Q7aW50ZXJ2YWwmcXVvdDs6ICZxdW90OzYwJnF1b3Q7LA0KICAgICAgJnF1b3Q7dGltZXpvbmUmcXVvdDs6ICZxdW90O0V0Yy9VVEMmcXVvdDssDQogICAgICAmcXVvdDt0aGVtZSZxdW90OzogJnF1b3Q7V2hpdGUmcXVvdDssDQogICAgICAmcXVvdDtzdHlsZSZxdW90OzogJnF1b3Q7MSZxdW90OywNCiAgICAgICZxdW90O3Rvb2xiYXJfYmcmcXVvdDs6ICZxdW90OyNmMWYzZjYmcXVvdDssDQogICAgICAmcXVvdDthbGxvd19zeW1ib2xfY2hhbmdlJnF1b3Q7OiB0cnVlLA0KICAgICAgJnF1b3Q7aGlkZWlkZWFzJnF1b3Q7OiB0cnVlLA0KICAgICAgJnF1b3Q7aGVpZ2h0JnF1b3Q7OjYwMDANCiAgICB9KTsNCiZsdDsvc2NyaXB0Jmd0Ow==');"></a></h6>
                    <div class="white-box-content">
                        <h6 class="head">{{__('dashboard.trade-status')}}</h6>
                        <span class="text">{{__('dashboard.waiting')}}</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="white-box small">
                    <h6 class="head-bk "><span class="round red"></span>EUR/JPY<a class="icon-graph" onclick="open_graph_popup('Jmx0O3NjcmlwdCB0eXBlPSZxdW90O3RleHQvamF2YXNjcmlwdCZxdW90OyZndDsNCiAgICBuZXcgVHJhZGluZ1ZpZXcud2lkZ2V0KHsNCiZxdW90O2NvbnRhaW5lcl9pZCZxdW90OzomcXVvdDtFVVJKUFkmcXVvdDssDQogICAgICAmcXVvdDthdXRvc2l6ZSZxdW90OzogdHJ1ZSwNCiAgICAgICZxdW90O3N5bWJvbCZxdW90OzogJnF1b3Q7Rlg6RVVSSlBZJnF1b3Q7LA0KICAgICAgJnF1b3Q7aW50ZXJ2YWwmcXVvdDs6ICZxdW90OzYwJnF1b3Q7LA0KICAgICAgJnF1b3Q7dGltZXpvbmUmcXVvdDs6ICZxdW90O0V0Yy9VVEMmcXVvdDssDQogICAgICAmcXVvdDt0aGVtZSZxdW90OzogJnF1b3Q7V2hpdGUmcXVvdDssDQogICAgICAmcXVvdDtzdHlsZSZxdW90OzogJnF1b3Q7MSZxdW90OywNCiAgICAgICZxdW90O3Rvb2xiYXJfYmcmcXVvdDs6ICZxdW90OyNmMWYzZjYmcXVvdDssDQogICAgICAmcXVvdDthbGxvd19zeW1ib2xfY2hhbmdlJnF1b3Q7OiB0cnVlLA0KICAgICAgJnF1b3Q7aGlkZWlkZWFzJnF1b3Q7OiB0cnVlDQogICAgfSk7DQombHQ7L3NjcmlwdCZndDs=');"></a></h6>
                    <div class="white-box-content">
                        <h6 class="head">{{__('dashboard.trade-status')}}</h6>
                        <span class="text">{{__('dashboard.waiting')}}</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="white-box small">
                    <h6 class="head-bk "><span class="round red"></span>USD/CAD<a class="icon-graph" onclick="open_graph_popup('Jmx0O3NjcmlwdCB0eXBlPSZxdW90O3RleHQvamF2YXNjcmlwdCZxdW90OyZndDsNCiAgICBuZXcgVHJhZGluZ1ZpZXcud2lkZ2V0KHsNCiZxdW90O2NvbnRhaW5lcl9pZCZxdW90OzomcXVvdDtVU0RDQUQmcXVvdDssDQogICAgICAmcXVvdDthdXRvc2l6ZSZxdW90OzogdHJ1ZSwNCiAgICAgICZxdW90O3N5bWJvbCZxdW90OzogJnF1b3Q7Rlg6VVNEQ0FEJnF1b3Q7LA0KICAgICAgJnF1b3Q7aW50ZXJ2YWwmcXVvdDs6ICZxdW90OzYwJnF1b3Q7LA0KICAgICAgJnF1b3Q7dGltZXpvbmUmcXVvdDs6ICZxdW90O0V0Yy9VVEMmcXVvdDssDQogICAgICAmcXVvdDt0aGVtZSZxdW90OzogJnF1b3Q7V2hpdGUmcXVvdDssDQogICAgICAmcXVvdDtzdHlsZSZxdW90OzogJnF1b3Q7MSZxdW90OywNCiAgICAgICZxdW90O3Rvb2xiYXJfYmcmcXVvdDs6ICZxdW90OyNmMWYzZjYmcXVvdDssDQogICAgICAmcXVvdDthbGxvd19zeW1ib2xfY2hhbmdlJnF1b3Q7OiB0cnVlLA0KICAgICAgJnF1b3Q7aGlkZWlkZWFzJnF1b3Q7OiB0cnVlDQogICAgfSk7DQombHQ7L3NjcmlwdCZndDs=');"></a></h6>
                    <div class="white-box-content">
                        <h6 class="head">{{__('dashboard.trade-status')}}</h6>
                        <span class="text">{{__('dashboard.waiting')}}</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="white-box small">
                    <h6 class="head-bk "><span class="round red"></span>GBP/JPY<a class="icon-graph" onclick="open_graph_popup('Jmx0O3NjcmlwdCB0eXBlPSZxdW90O3RleHQvamF2YXNjcmlwdCZxdW90OyZndDsNCiAgICBuZXcgVHJhZGluZ1ZpZXcud2lkZ2V0KHsNCiZxdW90O2NvbnRhaW5lcl9pZCZxdW90OzomcXVvdDtHQlBKUFkmcXVvdDssDQogICAgICAmcXVvdDthdXRvc2l6ZSZxdW90OzogdHJ1ZSwNCiAgICAgICZxdW90O3N5bWJvbCZxdW90OzogJnF1b3Q7Rlg6R0JQSlBZJnF1b3Q7LA0KICAgICAgJnF1b3Q7aW50ZXJ2YWwmcXVvdDs6ICZxdW90OzYwJnF1b3Q7LA0KICAgICAgJnF1b3Q7dGltZXpvbmUmcXVvdDs6ICZxdW90O0V0Yy9VVEMmcXVvdDssDQogICAgICAmcXVvdDt0aGVtZSZxdW90OzogJnF1b3Q7V2hpdGUmcXVvdDssDQogICAgICAmcXVvdDtzdHlsZSZxdW90OzogJnF1b3Q7MSZxdW90OywNCiAgICAgICZxdW90O3Rvb2xiYXJfYmcmcXVvdDs6ICZxdW90OyNmMWYzZjYmcXVvdDssDQogICAgICAmcXVvdDthbGxvd19zeW1ib2xfY2hhbmdlJnF1b3Q7OiB0cnVlLA0KICAgICAgJnF1b3Q7aGlkZWlkZWFzJnF1b3Q7OiB0cnVlDQogICAgfSk7DQombHQ7L3NjcmlwdCZndDs=');"></a></h6>
                    <div class="white-box-content">
                        <h6 class="head">{{__('dashboard.trade-status')}}</h6>
                        <span class="text">{{__('dashboard.waiting')}}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        @include('layouts.sidebar')

        <div class="col-md-12">
            <div class="white-box">
                <p align="justify">
                    <!-- TradingView Widget BEGIN -->
                </p><div id="tv-miniwidget-1b34d"><div id="tradingview_6708f-wrapper" style="position: relative;box-sizing: content-box;width: 100%;height: 400px;margin: 0 auto !important;padding: 0 !important;font-family:Arial,sans-serif;"><div style="width: 100%;height: 400px;background: #fff;padding: 0 !important;"><iframe id="tradingview_6708f" src="https://s.tradingview.com/miniwidgetembed/?Forex=FX%3AEURUSD,FX%3AGBPUSD,FX%3AUSDJPY,FX%3AUSDCHF,FX%3AAUDUSD,FX%3AUSDCAD&amp;tabs=Forex&amp;activeTickerBackgroundColor=%23EDF0F3&amp;trendLineColor=%234bafe9&amp;underLineColor=%23dbeffb&amp;fontColor=%2383888D&amp;gridLineColor=%23E9E9EA&amp;large_chart_url=https%3A%2F%2Fsecure.forexsignal.com%2Fchart.html&amp;width=100%25&amp;height=400px&amp;utm_source=secure.forexsignal.com&amp;utm_medium=widget&amp;utm_campaign=market-overview" width="100%" height="400px" frameborder="0" allowtransparency="true" scrolling="no" style="margin: 0 !important; padding: 0 !important;"></iframe></div><div style="position:absolute;display: block;box-sizing: content-box;height: 24px;width: 100%;bottom: 0;left: 0;margin: 0;padding: 0;font-family: Arial,sans-serif;"><div style="display: block;margin: 0 1px 1px 1px;line-height: 7px;box-sizing: content-box;height: 11px;padding: 6px 10px;text-align: right;background: #fff;"><a href="https://www.tradingview.com/?utm_source=secure.forexsignal.com&amp;utm_medium=widget&amp;utm_campaign=market-overview" target="_blank" style="color: #0099d4;text-decoration: none;font-size: 11px;"><span style="color: #b4b4b4;font-size: 11px;">Quotes by</span> TradingView</a></div></div></div></div>
                <script type="text/javascript" src="https://d33t3vvu2t2yu5.cloudfront.net/tv.js"></script>
                <script type="text/javascript">
                  new TradingView.MiniWidget({
                    "container_id": "tv-miniwidget-1b34d",
                    "tabs": [
                      "Forex"
                    ],
                    "symbols": {
                      "Forex": [
                        "FX:EURUSD",
                        "FX:GBPUSD",
                        "FX:USDJPY",
                        "FX:USDCHF",
                        "FX:AUDUSD",
                        "FX:USDCAD"
                      ]
                    },
                    "gridLineColor": "#E9E9EA",
                    "fontColor": "#83888D",
                    "underLineColor": "#dbeffb",
                    "trendLineColor": "#4bafe9",
                    "activeTickerBackgroundColor": "#EDF0F3",
                    "large_chart_url": "https://secure.forexsignal.com/chart.html",
                    "noGraph": false,
                    "width": "100%",
                    "height": "400px"
                  });
                </script>
                <!-- TradingView Widget END -->

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
            <p>
                {{__('dashboard.content')}}
                
            </p>
            </div>
        </div>
    </div>
</div>
@endsection
