@if(auth()->user()->paid)
  <link href="{{ asset('css/style.css') }}" rel="stylesheet">
<div id="mySidenav" class="sidenav">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <a class="dashboard" href="/home"><i class="icon"></i>{{__('sidemenu.signal')}}</a>
    <a class="calender" href="/calender"><i class="icon"></i>{{__('sidemenu.calendar')}}</a>
    <a class="meters" href="/trend-meters"><i class="icon"></i>{{__('sidemenu.meters')}}</a>
    <a class="news" href="/forex-news"><i class="icon"></i>{{__('sidemenu.news')}}</a>
    <a class="setting" href="/affiliate"><i class="icon"></i>{{__('sidemenu.affiliate')}}</a>
@if(auth()->user()->is_admin)
    <a class="sms" href="/sms"><i class="icon"></i>SMS</a>
    <a class="announcements" href="/announcements"><i class="icon"></i>{{__('sidemenu.announcements')}}</a>
@endif
</div>
@endif