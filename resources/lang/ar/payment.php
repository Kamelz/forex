<?php

return [

    'plan' => 'الباقة',
    'name' => 'الاسم',
    'email' => 'البريد الإلكترونى',
    'password' => 'كلمة المرور',
    'confirm' => 'تأكيد كلمة المرور',
    'address' => 'العنوان',
    'country' => 'البلد',
    'state' => 'المحافظة',
    'city' => 'المدينة',
    'zipcode' => 'كود المحافظة',
    'mobile' => 'الهاتف',
    'submit' => 'ادفع',
];
