<?php

return [

    'email' => 'E-Mail Address',
    'password' => 'Password',
    'remember' => ' Remember Me',
    'forgot' => 'Forgot Your Password?',
    'login' => 'Login',
    'logout' => 'Logout',
    'affiliate' => 'Affiliate',
];
