<?php

return [

    'plan' => 'Plan',
    'name' => 'Name',
    'email' => 'Email',
    'password' => 'Password',
    'confirm' => 'Confirm Password',
    'address' => 'Address',
    'country' => 'Country',
    'state' => 'State',
    'city' => 'City',
    'zipcode' => 'ZipCode',
    'mobile' => 'Mobile',
    'submit' => 'Submit Payment',
];
