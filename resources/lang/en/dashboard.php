<?php

return [

    'latest-news' => 'Latest Forex News',
    'reuters-news' => 'Reuters News',
    'forex-news' => 'Forex News',
    'affiliate-link' => 'Affiliate link',
    'up' => 'Trend Up',
    'down' => 'Trend Down',
    'update' => 'Updated',
    'open' => 'Open',
    'welcome' => 'Welcome!',
    'announcements' => 'Announcements',
    'no-announcements' => 'No announcements',
    'trade-status' => 'Trade Status',
    'waiting' => 'Waiting for New Opportunity',
    'content' =>'It is essential that a demo simulator account is first used THOROUGHLY before commencing with a Live Online Forex account. Trading foreign exchange carries a high level of risk, and may not be suitable for all investors. Before deciding to trade foreign exchange you should carefully consider your investment objectives, level of experience, and risk appetite. The possibility exists that you could sustain a loss of some or all of your initial investment and therefore you should not invest money that you cannot afford to lose. You should be aware of all the risks associated with foreign exchange trading, and seek advice from an independent financial advisor if you have any doubts. It should be understood that Currency trading involves high risk and you can lose a lot of money. There is always a relationship between high reward and high risk. Any type of market or trade speculation that can yield an unusually high return on investment is subject to unusually high risk. Only surplus funds should be placed at risk and anyone who does not have such funds should not participate in trading foreign currencies. Currency trading is not suitable for everyone.',

    'title' => 'Title',
    'body' => 'Body',
    'link' => 'Link',
    'note' => 'Note',
    'submit' => 'Submit',
    'name' => 'Name',
    'email' => 'Email',
    'number' => 'Number',
    'message' => 'Message',
    'send-to-all' => 'SEND TO ALL PAID USERS',
    'send' => 'send',
    'send-sms' => 'Send SMS',

];
