<?php

return [

    'n1' => 'US Dollar May Keep Falling on Cooling Fed Rate Hike Bets',
    's1' => 'The US Dollar may continue to decline as disappointing ADP jobs and ISM manufacturing d...',


    'n2' => 'Gold Prices May Extend Gains on Soft ADP, ISM Data',
    's2' => 'Gold prices may continue higher after hitting a one-month high as soft outcomes on meas...',

    'n3' => 'Yuan Soared to 7-Month High - What’s Next?',
    's3' => 'Here are all the technical and fundamental updates that you need to know about the Yuan.',

    'n4' => 'Cable Reversal Targets Monthly Open Ahead of NFPs, UK Elections',
    's4' => 'Today’s reversal off support leaves sterling looking to close the month virtually...',

    'n5' => 'Brexit Briefing: UK Polls Give GBP Traders a Boost',
    's5' => 'A poll showing next week’s General election resulting in a hung parliament sent G...',


    'n6' => 'EUR/USD to Resistance, GBP/USD to Support: Drivers on the Horizon',
    's6' => 'Two different themes are being seen across the English Channel, as EUR/USD continues it...',

    'n7' => 'Crude Oil Prices Retreat as Markets Await US Inventory Data',
    's7' => 'Crude oil prices are back on the defensive as markets await US inventory flow data in t...',

    'n8' => 'Euro May Shrug Off CPI Data, US Dollar at Risk on Fed Beige Book',
    's8' => 'The Euro may shrug off a drop in the regional inflation rate while the US Dollar may de...',

    'n9' => 'Euro, Yuan at Resistance ahead of EU-China Summit',
    's9' => 'Chinese Premier Li Keqiang will start his 3-day trip to Europe tomorrow. Why this is im...',

    'n10' => 'Gold Prices May Extend Gains on Soft US Economic Data',
    's10' => 'Gold prices may extend gains inspired by pre-election UK political uncertainty fears as...',

    'n11' => 'Euro and US Dollar Look Vulnerable as Inflation Slows',
    's11' => 'The Euro and the US Dollar may face selling pressure as German and US inflation measure...',


    'n12' => 'Euro May Fall as Draghi Argues for Firmly Dovish ECB',
    's12' => 'The Euro may fall as ECB President Mario Draghi pushes back against stimulus withdrawal...',


    'n40' => 'What Has Driven Bitcoin to Its Record-Breaking Rally? ',
    's40' => 'The cryptocurrency space’s most popular member – Bitcoin – continues ...',


    'n13' => 'Weekly Trading Forecast: Potent Event Risk Portends Volatility',
    's13' => 'Financial markets face a steady stream of heavy duty scheduled event risk that is likel...',


    'n14' => 'US Dollar Faces Trial By Fire as Top Tier Data Hits the Wires',
    's14' => 'The US Dollar faces a challenging week ahead as a steady stream of top-tier economic da...',


    'n15' => 'Australian Dollar May Wobble If Housing, China Data Disappoint',
    's15' => 'The Australian Dollar could be in for trouble in the week ahead if local housing data a...',


    'n16' => 'Yuan Eyes Chinese PMI after PBOC Revises Reference Rate Mechanism',
    's16' => 'China announced to add a “counter-cyclical” component to the Yuan fixing ca...',


    'n17' => 'Will European Inflation Keep the Taper Trade Running?',
    's17' => 'European data has driven strength into the currency despite the ECB’s continued i...',


    'n18' => 'British Pound: It All Depends on the Election Opinion Polls',
    's18' => 'After Friday’s surprise opinion poll showing the ruling Conservatives with just a...',


    'n19' => 'Oil: Might be Tough to Revisit the Recent Highs',
    's19' => 'The latest OPEC production cut extension sent oil lower, a potentially bearish signal.',


    'n20' => "China's yuan gains on PBOC's calming words, stock markets unconvinced",

    'n21' => "Forex - Dollar Dips in Thin Trade Ahead of U.S. Holiday",

    'n22' => "Forex – Yuan Stabilizes as PBOC Acts to Calm Markets",

    'n23' => "China's Yuan Shakeout Fails to Trigger Panic Among Traders",

    'n24' => "Forex - Dollar Lower on Euro Strength as Merkel Reaches Immigration Comprise",

    'n25' => "Mexico peso surges in wake of election on emerging market rally",

    'n26' => "Forex - Euro Pushes Higher, Dollar, Yen Slip Lower",

    'n27' => "Dollar dips, euro steadies after German coalition partners resolve row",

    'n28' => "Dollar Rises Ahead of Looming U.S., China Tariffs",
    
    'n29' => "China's state-owned banks seen supporting yuan as market rout deepens",
    
    'n30' => "Dollar dips as euro steadies after German coalition partners resolve row",
    
    'n31' => "Yuan Chases Loonie's Share as Bank of Russia Adds Chinese Asset",
    
    'n32' => "Argentine Peso Jumps After Central Bank Acts to Reverse Sentiment",

    'n33' => "UK PM May wants future customs relationship with EU by end of 2020",
    
    'n34' => "Forex - Dollar Up Sharply on Solid Manufacturing Activity, Trade Concerns",

    'n35' => "Trade Wars Bite as Emerging Currencies Decline to 10-Month Low",
    
    'n36' => "Forex - Euro Lower as Trade Fears, German Uncertainty Weigh",
    
    'n37' => "Mexico peso wobbles after leftist Lopez Obrador's big win",
    
    'n38' => "Euro hit by German coalition crisis, Mexican peso gains as Lopez Obrador wins",
    
    'n39' => "Mexican peso rallies as exit polls show Lopez Obrador win",
];
