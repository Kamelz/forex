<?php

return [


    'signal' => 'Signals Dashboard',
    'calendar' => 'Economic Calendar',
    'meters' => 'Trend Meters',
    'news' => 'Forex News',
    'affiliate' => 'Affiliate Program',
    'announcements' => 'Announcements',

];
